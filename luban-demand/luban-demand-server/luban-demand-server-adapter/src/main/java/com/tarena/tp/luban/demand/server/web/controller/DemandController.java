package com.tarena.tp.luban.demand.server.web.controller;

import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.service.DemandService;
import com.tarena.tp.luban.demand.server.web.assemble.RequestOrderAssemble;
import com.tarena.tp.luban.demand.server.web.vo.RequestOrderListItemVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.model.Result;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemandController {
    @Autowired
    private DemandService demandService;

    @Autowired
    private RequestOrderAssemble requestOrderAssemble;

    @PostMapping("/demand/order/search")
    public Result<PagerResult<RequestOrderListItemVO>> demandList(@RequestBody RequestOrderQuery requestOrderQuery) {
        ListRecordTotalBO<RequestOrderBO> recordTotalBO = demandService.demandList(requestOrderQuery);
        PagerResult<RequestOrderListItemVO> pagerResult = requestOrderAssemble.assemblePagerResult(recordTotalBO, requestOrderQuery);
        return new Result<>(pagerResult);
    }
}
