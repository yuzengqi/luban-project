/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.server.web.assemble;

import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.web.vo.RequestOrderListItemVO;
import com.tarena.tp.luban.demand.server.web.vo.RequestOrderVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.pager.PagerResult;
import com.tedu.inn.protocol.pager.SimplePagerQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class RequestOrderAssemble {

    public RequestOrderVO assembleBO2RequestOrderVO(RequestOrderBO bo) {
        RequestOrderVO requestOrder = new RequestOrderVO();
        BeanUtils.copyProperties(bo, requestOrder);
        requestOrder.setRequestOrderNo(bo.getRequestOrderNo());
        requestOrder.setCreateTime(bo.getGmtCreate());
        return requestOrder;
    }

    public RequestOrderListItemVO assembleBO2VO(RequestOrderBO bo) {
        RequestOrderListItemVO requestOrder = new RequestOrderListItemVO();
        BeanUtils.copyProperties(bo, requestOrder);
        requestOrder.setRequestOrderNo(bo.getRequestOrderNo());
        requestOrder.setCreateTime(bo.getGmtCreate());
        requestOrder.setCategoryId(bo.getOrderCategoryId());
        return requestOrder;
    }





    public List<RequestOrderListItemVO> boListAssembleVOList(List<RequestOrderBO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<RequestOrderListItemVO> requestOrderVOList = new ArrayList<>(list.size());
        for (RequestOrderBO requestOrderBO : list) {
            RequestOrderListItemVO requestOrderVO = this.assembleBO2VO(requestOrderBO);
            requestOrderVOList.add(requestOrderVO);
        }
        return requestOrderVOList;
    }

    public PagerResult<RequestOrderListItemVO> assemblePagerResult(ListRecordTotalBO<RequestOrderBO> accessProviderListTotalRecord,
                                                                   SimplePagerQuery requestOrderQuery) {
        List<RequestOrderListItemVO> requestOrderVOList = this.boListAssembleVOList(accessProviderListTotalRecord.getList());
        PagerResult<RequestOrderListItemVO> pagerResult = new PagerResult<>(requestOrderQuery);
        pagerResult.setObjects(requestOrderVOList);
        pagerResult.setTotal(accessProviderListTotalRecord.getTotal());
        return pagerResult;
    }
}
