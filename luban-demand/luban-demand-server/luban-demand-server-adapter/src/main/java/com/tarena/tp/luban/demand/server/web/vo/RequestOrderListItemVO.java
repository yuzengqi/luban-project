package com.tarena.tp.luban.demand.server.web.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderListItemVO {

    @ApiModelProperty("编号")
    private Long id;

    @ApiModelProperty("需求单编号")
    private String requestOrderNo;

    @ApiModelProperty("订单类型id")
    private Integer categoryId;

    @ApiModelProperty("订单类型中文")
    private String orderCategoryName;

    @ApiModelProperty("原始单价")
    private Long orderAmount;

    @ApiModelProperty("展示价格")
    private Long viewOrderAmount;

    @ApiModelProperty("分润比例")
    private BigDecimal profitScale;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("手机号")
    private String userPhone;

    @ApiModelProperty("上门详细地址")
    private String address;

    @ApiModelProperty("创建时间")
    private Long createTime;

    @ApiModelProperty("预约时间")
    private Long serviceTime;


}
