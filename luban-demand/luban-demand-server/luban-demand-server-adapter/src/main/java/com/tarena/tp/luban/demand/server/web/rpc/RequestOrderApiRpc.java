package com.tarena.tp.luban.demand.server.web.rpc;

import com.tarena.tp.luban.demand.server.client.RequestOrderApi;
import com.tarena.tp.luban.demand.server.service.DemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RequestOrderApiRpc implements RequestOrderApi {
    @Autowired
    private DemandService demandService;
    @Override
    public Boolean grabOrder(String requestOrderNo) {
        return demandService.grabOrder(requestOrderNo);
    }

    @Override
    public Boolean returnOrder(String requestOrderNo) {
        return null;
    }
}
