package com.tarena.tp.luban.demand.server.service;

import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.protocol.param.GrabOrderParam;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.repository.RequestOrderRepository;
import com.tedu.inn.protocol.ListRecordTotalBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class DemandService {
    @Autowired
    private RequestOrderRepository requestOrderRepository;

    public ListRecordTotalBO<RequestOrderBO> demandList(RequestOrderQuery requestOrderQuery) {
        //首先要获取地区和品类信息
        replenishQuery(requestOrderQuery);
        Long total = requestOrderRepository.getRequestOrderCount(requestOrderQuery);
        List<RequestOrderBO> requestOrderBOS = new ArrayList<>();
        if (total > 0) {
            requestOrderBOS = requestOrderRepository.queryRequestOrders(requestOrderQuery);
            // TODO 调整list中的数据--展示价格
            calculateViewAmount(requestOrderBOS);
        }
        return new ListRecordTotalBO<RequestOrderBO>(requestOrderBOS, total);
    }

    private void calculateViewAmount(List<RequestOrderBO> requestOrderBOS) {
        for (RequestOrderBO requestOrderBO : requestOrderBOS) {
            requestOrderBO.setProfitScale(new BigDecimal(20));
            //100 - 20
            //80 /100
            BigDecimal workerPercent = new BigDecimal(100).subtract(requestOrderBO.getProfitScale()).divide(new BigDecimal(100));
            Long viewOrderAmount = new BigDecimal(requestOrderBO.getOrderAmount()).multiply(workerPercent).longValue();
            requestOrderBO.setViewOrderAmount(viewOrderAmount);
        }
    }

    private void replenishQuery(RequestOrderQuery requestOrderQuery) {
        List<Integer> areaLists = new ArrayList<>();
        areaLists.add(1);
        List<Integer> cateLists = new ArrayList<>();
        cateLists.add(1);
        requestOrderQuery.setAreaIds(areaLists);
        requestOrderQuery.setOrderCategoryIds(cateLists);
    }

    public Boolean grabOrder(String requestOrderNo) {
        GrabOrderParam grabOrderParam = new GrabOrderParam();
        grabOrderParam.setRequestOrderNO(requestOrderNo);
        Integer result = requestOrderRepository.grabOrder(grabOrderParam);
        return result == 1;
    }
}
