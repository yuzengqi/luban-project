package com.tarena.tp.luban.demand.server.bo;

import io.github.classgraph.json.Id;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class RequestOrderBO{

    private Long id;

    private String requestOrderNo;

    private String orderName;

    private String orderType;

    private Integer orderCategoryId;

    private String orderCategoryName;

    private Long orderAmount;

    private Long viewOrderAmount;

    private BigDecimal  profitScale;

    private String userName;

    private String userPhone;

    private Integer areaId;

    private String address;

    private Long serviceTime;

    private Integer status;

    private Integer providerId;

    private String providerName;

    private String createUserName;

    private Long createUserId;

    private Long modifiedUserId;

    private String modifiedUserName;

    private Long gmtModified;

    private Long gmtCreate;

    private Integer version;
}
