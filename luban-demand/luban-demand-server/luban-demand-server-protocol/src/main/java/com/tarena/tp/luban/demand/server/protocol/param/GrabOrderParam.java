package com.tarena.tp.luban.demand.server.protocol.param;

import lombok.Data;

@Data
public class GrabOrderParam {
    private String requestOrderNO;
    private Integer version;
}
