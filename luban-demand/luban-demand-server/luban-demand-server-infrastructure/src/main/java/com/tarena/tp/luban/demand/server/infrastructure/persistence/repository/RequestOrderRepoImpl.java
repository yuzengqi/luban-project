package com.tarena.tp.luban.demand.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.demand.po.RequestOrder;
import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.dao.RequestOrderDao;
import com.tarena.tp.luban.demand.server.infrastructure.persistence.data.converter.RequestOrderConverter;
import com.tarena.tp.luban.demand.server.protocol.param.GrabOrderParam;
import com.tarena.tp.luban.demand.server.protocol.param.RequestOrderParam;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.repository.RequestOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RequestOrderRepoImpl implements RequestOrderRepository {
    @Autowired(required = false)
    private RequestOrderDao requestOrderDao;

    @Autowired
    private RequestOrderConverter requestOrderConverter;

    @Override
    public Long save(RequestOrderParam requestOrderParam) {
        return null;
    }

    @Override
    public Integer grabOrder(GrabOrderParam grabOrderParam) {
        //还需要使用乐观锁
        //还要更新订单信息
        //要返回result，如果抢单成功返回1
        RequestOrder requestOrder = requestOrderDao.getRequestOrderByOrderNo(grabOrderParam.getRequestOrderNO());
        Integer result = 0;
        //如果查得到
        if(requestOrder != null){
            grabOrderParam.setVersion(requestOrder.getVersion());
            result = requestOrderDao.grabOrderCas(grabOrderParam);
        }
        return result;
    }

    @Override
    public Integer cancelGrabOrder(String requestOrderNo) {
        return null;
    }

    @Override
    public RequestOrderBO getRequestOrder(String requestOrderNo) {
        return null;
    }

    @Override
    public List<RequestOrderBO> queryRequestOrders(RequestOrderQuery requestOrderQuery) {
        List<RequestOrder> requestOrders = requestOrderDao.queryRequestOrders(requestOrderConverter.toDbPagerQuery(requestOrderQuery));
        List<RequestOrderBO> requestOrderBOS = requestOrderConverter.poList2BoList(requestOrders);
        return requestOrderBOS;
    }

    @Override
    public Long getRequestOrderCount(RequestOrderQuery requestOrderQuery) {
        return requestOrderDao.countRequestOrder(requestOrderConverter.toDbPagerQuery(requestOrderQuery));
    }

    @Override
    public Integer returnOrder(GrabOrderParam requestOrderNo) {
        return null;
    }

    @Override
    public RequestOrderBO getRequestOrderGrabed(String requestOrderNo) {
        return null;
    }
}
