package com.tarena.tp.luban.demand.po;

import com.tedu.inn.protocol.dao.PO;
import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "provider_audit_log")
public class ProviderAuditLog extends PO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)", nullable = false)
    private Long id;

    @Column(name = "user_id", columnDefinition = "int(10) DEFAULT 0 COMMENT '审核人Id'", nullable = false)
    private Long userId;

    @Column(name = "user_name", columnDefinition = "varchar(16) DEFAULT '' COMMENT '审核人姓名'", nullable = false)
    private String userName;

    @Column(name = "provider_id", columnDefinition = "int(10) DEFAULT 0 COMMENT '供应商id'", nullable = false)
    private Long providerId;

    @Column(name = "operate_time", columnDefinition = "bigint(20) DEFAULT 0 COMMENT '操作时间'", nullable = false)
    private Long operateTime;

    @Column(name = "operate_name", columnDefinition = "varchar(16) DEFAULT '' COMMENT '操作步骤'", nullable = false)
    private String operateName;

    @Column(name = "audit_status", columnDefinition = "TINYINT(2) DEFAULT 0 COMMENT '审核状态'", nullable = false)
    private Integer auditStatus;

    @Column(name = "reject_reason", columnDefinition = "varchar(64) DEFAULT '' COMMENT '驳回原因'", nullable = false)
    private String rejectReason;

    @Column(name = "remark", columnDefinition = "varchar(64) DEFAULT '' COMMENT '备注'", nullable = false)
    private String remark;
}
