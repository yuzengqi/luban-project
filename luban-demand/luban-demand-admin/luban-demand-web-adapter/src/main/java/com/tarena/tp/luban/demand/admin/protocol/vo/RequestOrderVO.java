package com.tarena.tp.luban.demand.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderVO {

    @ApiModelProperty("编号")
    private Long id;

    @ApiModelProperty("需求单编号")
    private String requestOrderNo;

    @ApiModelProperty("订单名称")
    private String orderName;

    @ApiModelProperty("订单类型")
    private Integer categoryId;

    @ApiModelProperty("单价")
    private Long orderAmount;

    @ApiModelProperty("供应商名称")
    private Long providerName;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("手机号")
    private String userPhone;

    @ApiModelProperty("上门详细地址")
    private String address;

    @ApiModelProperty("创建时间")
    private Long gmtCreate;

}
