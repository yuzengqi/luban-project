package com.tarena.tp.luban.demand.admin.protocol.vo;

import com.tarena.tp.luban.demand.admin.bo.ProviderAuditLogBO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProviderAuditLogVO {

    @ApiModelProperty("供应商名称")
    private String ProviderName;
    private String principal;
    private String principalTel;
    private BigDecimal profitScale;
    private String address;
    private String businessLicense;
    private String description;


    @ApiModelProperty("审核日志")
    List<ProviderAuditLogBO> providerAuditLog;

}
