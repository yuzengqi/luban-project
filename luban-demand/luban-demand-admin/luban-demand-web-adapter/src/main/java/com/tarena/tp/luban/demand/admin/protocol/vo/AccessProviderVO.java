/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccessProviderVO {


 @ApiModelProperty("供应商名称")
 private String providerName;

 @ApiModelProperty("负责人")
 private String principal;

 @ApiModelProperty("负责人电话")
 private String principalTel;

 @ApiModelProperty("公司地址")
 private String address;

 @ApiModelProperty("营业执照")
 private String attachUrl;

 @ApiModelProperty("供应商简介")
 private String description;

 @ApiModelProperty("分润比例")
 private BigDecimal profitScale;

 @ApiModelProperty("审核状态 0:待审核,1:审核通过")
 private Integer auditStatus;


 @ApiModelProperty("更新人")
 private String modifiedUserName;

 @ApiModelProperty("更新时间")
 private Long gmtModified;

 @ApiModelProperty("审核记录")
 private List<ProviderAuditLogVO> providerAuditLog;

}