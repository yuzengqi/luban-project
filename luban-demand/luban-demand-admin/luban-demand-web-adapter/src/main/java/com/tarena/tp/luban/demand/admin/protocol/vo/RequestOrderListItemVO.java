package com.tarena.tp.luban.demand.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderListItemVO {

    @ApiModelProperty("编号")
    private Long id;

    @ApiModelProperty("需求单编号")
    private String requestOrderNo;

    @ApiModelProperty("订单名称")
    private String orderName;

    @ApiModelProperty("订单类型id")
    private Integer orderCategoryId;

    @ApiModelProperty("订单类型中文")
    private String orderCategoryName;

    @ApiModelProperty("单价")
    private Long orderAmount;

    @ApiModelProperty("用户名")
    private String userName;

    @ApiModelProperty("手机号")
    private String userPhone;

    @ApiModelProperty("上门详细地址")
    private String address;

    @ApiModelProperty("创建时间")
    private Long gmtCreate;

    @ApiModelProperty("抢单状态 0:待抢 1:已抢")
    private Integer grabStatus;

}
