/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.manager;


import com.tarena.tp.luban.demand.admin.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.admin.protocol.param.RequestOrderParam;
import com.tarena.tp.luban.demand.admin.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.admin.repository.RequestOrderRepository;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestOrderService {

    @Autowired
    private RequestOrderRepository requestOrderRepository;


    public RequestOrderBO getRequestOrder(Long requestOrderId) throws BusinessException {
        return this.requestOrderRepository.getRequestOrder(requestOrderId);
    }

    public ListRecordTotalBO<RequestOrderBO> queryRequestOrder(
            RequestOrderQuery requestOrderQuery) {
        Long totalRecord = this.requestOrderRepository.getRequestOrderCount(requestOrderQuery);
        List<RequestOrderBO> requestOrderBoList = new ArrayList<>();
        if (totalRecord > 0) {
            requestOrderBoList = this.requestOrderRepository.queryRequestOrders(requestOrderQuery);
        }
        return new ListRecordTotalBO<>(requestOrderBoList, totalRecord);
    }

}
