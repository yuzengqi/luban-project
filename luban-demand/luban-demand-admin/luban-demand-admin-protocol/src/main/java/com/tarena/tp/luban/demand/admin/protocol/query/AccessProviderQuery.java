/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.protocol.query;

import com.tedu.inn.protocol.pager.SimplePagerQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccessProviderQuery extends SimplePagerQuery {

    @ApiModelProperty("需求供应商")
    private String providerName;

    @ApiModelProperty("负责人")
    private String principal;

    @ApiModelProperty("负责人电话")
    private String principalTel;

    @ApiModelProperty("审核状态 0:待审核 1:审核通过 2:驳回")
    private List<Integer> auditStatus;

    @ApiModelProperty("启用状态 0:未启用,1:启用")
    private Integer status;

    @ApiModelProperty("下单开始日期")
    private Long beginDate;

    @ApiModelProperty("下单结束日期")
    private Long endDate;



}