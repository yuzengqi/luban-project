package com.tarena.tp.luban.demand.admin.protocol.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProviderAuditLogParam {

    @ApiModelProperty("供应商id")
    private Long providerId;

    @ApiModelProperty("供应商状态")
    private Integer auditStatus;

    @ApiModelProperty("拒绝原因")
    private String rejectReason;

    @ApiModelProperty("备注")
    private String remark;
}
