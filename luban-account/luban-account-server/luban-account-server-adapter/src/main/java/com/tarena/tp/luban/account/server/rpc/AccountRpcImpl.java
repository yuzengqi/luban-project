package com.tarena.tp.luban.account.server.rpc;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.server.assemble.AccountAssemble;
import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.dto.AccountDTO;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//这个类是暴露出来的接口实现类
@Component
public class AccountRpcImpl implements AccountApi {
    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountAssemble accountAssemble;

    @Override
    public Long create(AccountParam accountCreateParam) {
        return accountService.create(accountCreateParam);
    }

    @Override
    public AccountDTO getAccountByUserId(Long userId) {
        AccountBO accountBO = accountService.getAccountByUserId(userId);
        AccountDTO accountDTO = accountAssemble.assembleAccountDTO(accountBO);
        return accountDTO;
    }

    @Override
    public Long mockPayment(PaymentParam param) {
        return accountService.mockPay(param);
    }
}
