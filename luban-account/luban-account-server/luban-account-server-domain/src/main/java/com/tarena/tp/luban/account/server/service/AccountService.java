package com.tarena.tp.luban.account.server.service;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    public Long create(AccountParam accountCreateParam) {
        //如果账户存在返回-1
        AccountBO accountBO = accountRepository.getAccountByUserId(accountCreateParam.getUserId());
        if (accountBO == null || accountBO.getTotalAmount() == null) {
            //说明账户为空，需要创建账户
            Long aLong = accountRepository.create(accountCreateParam);
            return aLong;
        }
        return -1L;
    }

    public AccountBO getAccountByUserId(Long userId) {
        AccountBO accountBO = accountRepository.getAccountByUserId(userId);
        return accountBO;
    }

    public Long mockPay(PaymentParam param) {
        return accountRepository.updateAmount(param);
    }
}
