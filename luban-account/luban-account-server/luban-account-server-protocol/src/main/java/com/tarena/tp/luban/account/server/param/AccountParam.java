package com.tarena.tp.luban.account.server.param;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountParam implements Serializable {
    //师傅绑定的用户
    Long userId;
    //师傅名称
    String userName;
    //师傅手机号
    String userPhone;
}
