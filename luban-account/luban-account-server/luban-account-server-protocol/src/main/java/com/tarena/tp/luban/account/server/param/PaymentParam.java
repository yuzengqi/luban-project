package com.tarena.tp.luban.account.server.param;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PaymentParam implements Serializable {
    //给哪个师傅打款的 userId
    Long userId;
    //款项来自于哪个订单
    String orderNo;
    //打多少钱
    Long totalAmount;
}
