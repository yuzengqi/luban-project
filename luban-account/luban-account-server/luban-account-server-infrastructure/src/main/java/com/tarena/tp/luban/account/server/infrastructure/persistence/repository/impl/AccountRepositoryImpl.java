package com.tarena.tp.luban.account.server.infrastructure.persistence.repository.impl;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.po.Account;
import com.tarena.tp.luban.account.server.dao.AccountDAO;
import com.tarena.tp.luban.account.server.infrastructure.persistence.data.converter.AccountConverter;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public class AccountRepositoryImpl implements AccountRepository {
    @Autowired(required = false)
    private AccountDAO accountDAO;

    @Autowired
    private AccountConverter accountConverter;

    @Override
    public Long create(AccountParam accountCreateParam) {
        Account account = accountConverter.param2po(accountCreateParam);
        Long insert = accountDAO.insert(account);
        return insert;
    }

    @Override
    public AccountBO getAccountByUserId(Long userId) {
        Account account = accountDAO.getAccountByUserId(userId);
        AccountBO accountBO = accountConverter.po2bo(account);
        return accountBO;
    }

    @Override
    public Long updateAmount(PaymentParam param) {
        Account account = accountConverter.param2po(param);
        return accountDAO.updateAmount(account);
    }
}
