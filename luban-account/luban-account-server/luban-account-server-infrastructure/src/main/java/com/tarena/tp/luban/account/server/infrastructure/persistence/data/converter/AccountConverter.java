/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.server.infrastructure.persistence.data.converter;

import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.po.Account;
import com.tarena.tp.luban.account.server.dto.AccountDTO;
import java.util.ArrayList;
import java.util.List;

import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tedu.inn.protocol.enums.StatusRecord;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class AccountConverter {
    public AccountDTO convert(Account bank) {
        AccountDTO bankDTO = new AccountDTO();
        BeanUtils.copyProperties(bank, bankDTO);
        return bankDTO;
    }

    public List<AccountDTO> convertDtoList(List<Account> banks) {
        if (CollectionUtils.isEmpty(banks)) {
            return null;
        }
        List<AccountDTO> bankDTOs = new ArrayList<AccountDTO>();
        for (Account bank : banks) {
            bankDTOs.add(convert(bank));
        }
        return bankDTOs;
    }

    public Account param2po(PaymentParam param) {
        Account account = new Account();
        BeanUtils.copyProperties(param, account);
        LoginUser loginUser = SecurityContext.getLoginToken();
        account.setGmtCreate(System.currentTimeMillis());
        account.setGmtModified(System.currentTimeMillis());
        account.setCreateUserId(loginUser != null ? loginUser.getUserId() : 0);
        account.setModifiedUserId(loginUser != null ? loginUser.getUserId() : 0);
        account.setCreateUserName(loginUser != null ? loginUser.getUserName() : "mock");
        account.setModifiedUserName(loginUser != null ? loginUser.getUserName() : "mock");
        account.setStatus(StatusRecord.ENABLE.getStatus());
        return account;
    }

    public Account param2po(AccountParam param) {
        Account account = new Account();
        BeanUtils.copyProperties(param, account);
        LoginUser loginUser = SecurityContext.getLoginToken();
        account.setGmtCreate(System.currentTimeMillis());
        account.setGmtModified(System.currentTimeMillis());
        account.setCreateUserId(loginUser != null ? loginUser.getUserId() : 0);
        account.setModifiedUserId(loginUser != null ? loginUser.getUserId() : 0);
        account.setCreateUserName(loginUser != null ? loginUser.getUserName() : "mock");
        account.setModifiedUserName(loginUser != null ? loginUser.getUserName() : "mock");
        account.setStatus(StatusRecord.ENABLE.getStatus());
        account.setTotalAmount(0L);
        return account;
    }

    public AccountBO po2bo(Account account) {
        if (account == null) {
            return null;
        }
        AccountBO bo = new AccountBO();
        BeanUtils.copyProperties(account, bo);
        return bo;
    }
}
