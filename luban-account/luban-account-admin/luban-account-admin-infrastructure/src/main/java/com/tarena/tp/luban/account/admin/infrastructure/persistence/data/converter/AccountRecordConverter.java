/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.infrastructure.persistence.data.converter;

import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.account.admin.bo.AccountRecordBO;
import com.tarena.tp.luban.account.po.AccountRecord;
import com.tarena.tp.luban.account.admin.protocol.param.AccountRecordParam;
import com.tarena.tp.luban.account.admin.protocol.query.AccountRecordQuery;
import com.tarena.tp.luban.account.admin.dao.query.AccountRecordDBPagerQuery;
import org.springframework.beans.BeanUtils;
import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class AccountRecordConverter{

   public AccountRecordDBPagerQuery toDbPagerQuery(AccountRecordQuery accountRecordQuery) {
           if (accountRecordQuery == null) {
               return new AccountRecordDBPagerQuery();
           }
           AccountRecordDBPagerQuery accountRecordDBPagerQuery = new AccountRecordDBPagerQuery();
           BeanUtils.copyProperties(accountRecordQuery, accountRecordDBPagerQuery);
           return accountRecordDBPagerQuery;
       }
   

     public AccountRecord param2po(AccountRecordParam param) {
        AccountRecord accountRecord = new AccountRecord();
        BeanUtils.copyProperties(param, accountRecord);

        LoginUser loginUser=SecurityContext.getLoginToken();

        accountRecord.setGmtCreate(System.currentTimeMillis());
        accountRecord.setGmtModified(accountRecord.getGmtCreate());
        accountRecord.setCreateUserId(loginUser.getUserId());
        accountRecord.setModifiedUserId(loginUser.getUserId());
        accountRecord.setStatus(1);

        accountRecord.setCreateUserName(loginUser.getUserName());
        accountRecord.setModifiedUserName(loginUser.getUserName());
        return accountRecord;
    }

     public AccountRecordBO po2bo(AccountRecord accountRecord) {
        AccountRecordBO accountRecordBO = new AccountRecordBO();
        BeanUtils.copyProperties(accountRecord, accountRecordBO);
        return accountRecordBO;
    }

     public List<AccountRecordBO> poList2BoList(List<AccountRecord> list) {
        List<AccountRecordBO> accountRecordBos = new ArrayList<>(list.size());
        for (AccountRecord accountRecord : list) {
            accountRecordBos.add(this.po2bo(accountRecord));
        }
        return accountRecordBos;
    }

    public void convertStatus(StatusCriteria statusCriteria){
            LoginUser loginUser = SecurityContext.getLoginToken();
            statusCriteria.setModifiedUserId(loginUser.getUserId());
            statusCriteria.setModifiedUserName(loginUser.getUserName());
            statusCriteria.setGmtModified(System.currentTimeMillis());
    }
}