/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.infrastructure.persistence.data.converter;

import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.account.admin.bo.BankCardBO;
import com.tarena.tp.luban.account.po.BankCard;
import com.tarena.tp.luban.account.admin.protocol.param.BankCardParam;
import com.tarena.tp.luban.account.admin.protocol.query.BankCardQuery;
import com.tarena.tp.luban.account.admin.dao.query.BankCardDBPagerQuery;
import org.springframework.beans.BeanUtils;
import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class BankCardConverter{

   public BankCardDBPagerQuery toDbPagerQuery(BankCardQuery bankCardQuery) {
           if (bankCardQuery == null) {
               return new BankCardDBPagerQuery();
           }
           BankCardDBPagerQuery bankCardDBPagerQuery = new BankCardDBPagerQuery();
           BeanUtils.copyProperties(bankCardQuery, bankCardDBPagerQuery);
           return bankCardDBPagerQuery;
       }
   

     public BankCard param2po(BankCardParam param) {
        BankCard bankCard = new BankCard();
        BeanUtils.copyProperties(param, bankCard);

        LoginUser loginUser=SecurityContext.getLoginToken();

        bankCard.setGmtCreate(System.currentTimeMillis());
        bankCard.setGmtModified(bankCard.getGmtCreate());
        bankCard.setCreateUserId(loginUser.getUserId());
        bankCard.setModifiedUserId(loginUser.getUserId());
        bankCard.setStatus(1);

        bankCard.setCreateUserName(loginUser.getUserName());
        bankCard.setModifiedUserName(loginUser.getUserName());
        return bankCard;
    }

     public BankCardBO po2bo(BankCard bankCard) {
        BankCardBO bankCardBO = new BankCardBO();
        BeanUtils.copyProperties(bankCard, bankCardBO);
        return bankCardBO;
    }

     public List<BankCardBO> poList2BoList(List<BankCard> list) {
        List<BankCardBO> bankCardBos = new ArrayList<>(list.size());
        for (BankCard bankCard : list) {
            bankCardBos.add(this.po2bo(bankCard));
        }
        return bankCardBos;
    }

    public void convertStatus(StatusCriteria statusCriteria){
            LoginUser loginUser = SecurityContext.getLoginToken();
            statusCriteria.setModifiedUserId(loginUser.getUserId());
            statusCriteria.setModifiedUserName(loginUser.getUserName());
            statusCriteria.setGmtModified(System.currentTimeMillis());
    }
}