/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.po;

import com.tedu.inn.protocol.dao.PO;
import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "account_record")
public class AccountRecord extends PO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)")
    /**
     * 主键
     */
    private Long id;

    /**
     * 用户id
     */
    @Column(name = "user_id", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '用户id'", updatable = false)
    private Long userId;

    /**
     * 订单编号
     */
    @Column(name = "order_no", columnDefinition = "varchar(16) DEFAULT '' COMMENT '订单编号'", updatable = false)
    private String orderNo;

    /**
     * 结算单编号
     */
    @Column(name = "settlement_no", columnDefinition = "varchar(16) DEFAULT '' COMMENT '结算单编号'", updatable = false)
    private String settlementNo;


    /**
     * 当前帐户总收入
     */
    @Column(name = "total_amount", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '修改人id'")
    private Long totalAmount;

    /**
     * 当前帐户总收入
     */
    @Column(name = "amount", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '入账金额'")
    private Long amount;

    /**
     * 是否有效状态
     */
    @Column(name = "status", columnDefinition = "tinyint(1)  DEFAULT 0 COMMENT '状态 0:无效，1:有效'")
    private Integer status;
}
