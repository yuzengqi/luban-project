package com.tarena.tp.luban.gateway.error;

import com.tedu.inn.protocol.enums.ErrorSupport;
import lombok.Data;

public enum GatewayErrorEnum implements ErrorSupport {
    GATEWAY_ERROR("5001","gateway transfer error happened");
    GatewayErrorEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String code;

    private String message;

    @Override public String getCode() {
        return this.code;
    }

    @Override public String getMessage() {
        return this.message;
    }
}
