package com.tarena.tp.luban.gateway.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarena.passport.auto.domain.JwtRSAGenerator;
import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.protocol.PassportBusinessException;
import com.tarena.passport.protocol.enums.ErrorEnum;
import com.tedu.inn.protocol.model.Result;
import java.net.URI;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
@Component
@ConfigurationProperties(prefix = "gateway")
public class AuthenticationFilter implements GlobalFilter, Ordered {
    private Map<String, List<String>> whitelists;

    public Map<String, List<String>> getWhitelists() {
        return whitelists;
    }

    public void setWhitelists(Map<String, List<String>> whitelists) {
        this.whitelists = whitelists;
    }

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    @Autowired
    private JwtRSAGenerator<LoginUser> jwtRSAGenerator;
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //暂时屏蔽
        /*if(true){
            return chain.filter(exchange);
        }*/
        log.debug("进入过滤器");
        Route route = (Route) exchange.getAttribute(ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR);
        String routeId = null;
        if (route == null) {
            return chain.filter(exchange);
        } else {
            routeId = route.getId();
            log.debug("get route id :{}", routeId);
        }
        if (routeId != null) {
            //判断是否当前路径需要认证
            ServerHttpRequest request = exchange.getRequest();
            //拿到routeid
            if (whitelists == null || whitelists.size() == 0) {
                log.debug("this application does not contain any white uris");
                //没有白名单,所有请求均需要认证
            } else {
                log.debug("this gatewayapplication does contain some white uris");
                URI uri = request.getURI();
                boolean containsHostName = whitelists.containsKey(routeId);
                if (containsHostName) {
                    List<String> whiteUris = whitelists.get(routeId);
                    List<String> collect = whiteUris.stream().filter(matched -> {
                        return AuthenticationFilter.PATH_MATCHER.match(matched, uri.getPath());
                    }).collect(Collectors.toList());
                    //如果有匹配,返回结果不为空,表示当前uri在白名单中,放行
                    if (collect.size() != 0) {
                        return chain.filter(exchange);
                    }
                }
            }
        }
        //get jwt from the request
        try {
            log.debug("begin to get jwt");
            String jwt = getJwt(exchange);
            LoginUser token = jwtRSAGenerator.getLoginFromToken(jwt, LoginUser.class);
            String json = OBJECT_MAPPER.writeValueAsString(token);
            //{"":"","":"","":""}
            //向后传递头
            LinkedHashMap<String, String> headers = new LinkedHashMap<>();
            headers.put(AUTH_HEADER, json);
            chainFilterAndSetHeaders(chain, exchange, headers);
            return chain.filter(exchange);
        } catch (PassportBusinessException e) {
            ServerHttpResponse response = exchange.getResponse();
            Result fail = new Result(ErrorEnum.JWT_NOT_FOUND);
            log.error("网关出现jwt解析问题:{}", e.getMessage());
            response.setStatusCode(HttpStatus.OK);
            try {
                byte[] bytes = OBJECT_MAPPER.writeValueAsBytes(fail);
                DataBuffer wrap = response.bufferFactory().wrap(bytes);
                return response.writeWith(Mono.just(wrap));
            } catch (JsonProcessingException ex) {
                log.error("Json Processing EX:{},stack trace:{}", e.getMessage(), e.getStackTrace().toString());
                return chain.filter(exchange);
            }
        } catch (Exception e) {
            log.error("EX:{},stack trace:{}", e.getMessage(), e.getStackTrace());
            return chain.filter(exchange);
        }
    }

    @Override public int getOrder() {
        return 0;
    }

    //设置向后传递的header
    private Mono<Void> chainFilterAndSetHeaders(GatewayFilterChain chain, ServerWebExchange exchange,
        LinkedHashMap<String, String> headerMap) {
        // 添加header
        Consumer<HttpHeaders> httpHeaders = httpHeader -> {
            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                // 遍历Map设置header，向后传递
                httpHeader.set(entry.getKey(), entry.getValue());
            }
        };
        ServerHttpRequest newRequest = exchange.getRequest().mutate().headers(httpHeaders).build();
        ServerWebExchange build = exchange.mutate().request(newRequest).build();
        //将现在的request 变成 exchange对象
        return chain.filter(build);
    }

    private static final String AUTH_HEADER = "Authorization";
    private static final String BEARER = "Bearer";
    private static final String QUERY_TOKEN_PARAM = "token";

    private String getJwt(ServerWebExchange exchange) throws PassportBusinessException {
        HttpHeaders headers = exchange.getRequest().getHeaders();
        boolean authorization = headers.containsKey(AUTH_HEADER);
        List<String> auths = headers.get(AUTH_HEADER);
        log.debug("auth header contains:{}", auths == null ? null : auths);
        if (authorization && auths != null && auths.size() != 0) {
            log.debug("Auth header value:{}",headers.get(AUTH_HEADER));
            return headers.get(AUTH_HEADER).get(0).substring("Bearer ".length());
        } else {
            log.debug("jwt is not contained in Authorization header");
        }
        boolean bearer = headers.containsKey(BEARER);
        List<String> bearers = headers.get(BEARER);
        log.debug("bearer header contains:{}", bearers == null ? null : bearers);
        if (bearer && bearers != null && bearers.size() != 0) {
            return headers.get("Bearer").get(0);
        } else {
            log.debug("jwt is not contained in Bearer header");
        }
        MultiValueMap<String, String> params = exchange.getRequest().getQueryParams();
        boolean token = params.containsKey(QUERY_TOKEN_PARAM);
        if (!token) {
            log.debug("jwt is not contained by params");
            throw new PassportBusinessException(ErrorEnum.JWT_NOT_FOUND);
        } else {
            return params.getFirst(QUERY_TOKEN_PARAM);
        }

    }
}
