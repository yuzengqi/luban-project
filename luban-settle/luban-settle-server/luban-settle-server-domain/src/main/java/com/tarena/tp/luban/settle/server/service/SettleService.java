package com.tarena.tp.luban.settle.server.service;

import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.settle.server.assemble.SettleAssembler;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.server.repository.SettleBillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SettleService {
    @Autowired
    private SettleBillRepository settleBillRepository;

    @Autowired
    private SettleAssembler settleAssembler;

    @Autowired(required = false)
    private AccountApi accountApi;

    @Autowired
    private MessageService messageService;

    public void settle(OrderMqDTO orderMqDTO) {
        //防止重复消费
        //先获取orderNo的size
        Long size = settleBillRepository.getSettleBillByOrderNo(orderMqDTO.getOrderNo());
        //说明已经消费过了
        if (size > 0) {
            return;
        }
        List<SettleBillParam> settleBillParams = settleAssembler.order2Params(orderMqDTO);
        settleBillRepository.batchSave(settleBillParams);
        //接下来模拟打款
        PaymentParam paymentParam = new PaymentParam();
        paymentParam.setOrderNo(orderMqDTO.getOrderNo());
        paymentParam.setUserId(orderMqDTO.getUserId());
        paymentParam.setTotalAmount(settleBillParams.get(0).getTotalAmount());
        accountApi.mockPayment(paymentParam);
        messageService.sendSettledMessage(orderMqDTO.getOrderNo());
    }
}
