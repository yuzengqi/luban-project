package com.tarena.tp.luban.settle.server.assemble;

import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SettleAssembler {
    public List<SettleBillParam> order2Params(OrderMqDTO order) {
        SettleBillParam workerParam = new SettleBillParam();
        BeanUtils.copyProperties(order, workerParam);
        workerParam.setOrderGmtCreate(order.getGmtCreate());
        workerParam.setType(1);
        workerParam.setRequestUserPhone(order.getUserPhone());
        //没有userId
        workerParam.setRequestUserId(1000L);
        workerParam.setRequestUserName(order.getUserName());
        workerParam.setScale(order.getProfitScale());
        workerParam.setAmount(order.getRequestOrderPrice());
        workerParam.setTotalAmount(order.getRequestOrderPrice());
        workerParam.setStatus(1);
        Long payTime = System.currentTimeMillis();
        workerParam.setPaymentTime(payTime);
        //设置plate
        SettleBillParam plateParam = new SettleBillParam();
        BeanUtils.copyProperties(workerParam, plateParam);
        plateParam.setUserId(999L);
        plateParam.setType(2);
        plateParam.setAmount(order.getRequestOrderRawPrice() == null ? 100L - workerParam.getRequestOrderPrice() : order.getRequestOrderRawPrice() - workerParam.getRequestOrderPrice());
        plateParam.setTotalAmount(plateParam.getAmount());
        List<SettleBillParam> settles = new ArrayList<>();
        settles.add(workerParam);
        settles.add(plateParam);
        return settles;
    }
}
