package com.tarena.tp.luban.settle.server.web.consumer;

import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.settle.server.service.SettleService;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(
        topic = "order_finish_topic",
        consumerGroup = "settle_group1")
public class OrderFinishConsumer implements RocketMQListener<OrderMqDTO> {
    @Autowired
    private SettleService settleService;

    @Override
    public void onMessage(OrderMqDTO orderMqDTO) {
        settleService.settle(orderMqDTO);
    }
}
