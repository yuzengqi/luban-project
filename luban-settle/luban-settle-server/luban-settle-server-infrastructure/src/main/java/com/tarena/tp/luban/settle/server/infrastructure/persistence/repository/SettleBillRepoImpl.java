package com.tarena.tp.luban.settle.server.infrastructure.persistence.repository;

import com.github.xiaoymin.knife4j.core.util.CollectionUtils;
import com.tarena.tp.luban.settle.po.SettleBill;
import com.tarena.tp.luban.settle.server.dao.SettleBillDAO;
import com.tarena.tp.luban.settle.server.infrastructure.persistence.converter.SettleBillConverter;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.server.repository.SettleBillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SettleBillRepoImpl implements SettleBillRepository {
    @Autowired(required = false)
    private SettleBillDAO settleBillDAO;
    @Autowired
    private SettleBillConverter settleBillConverter;

    @Override
    public void batchSave(List<SettleBillParam> settleBillParams) {
        //params转化成pos
        List<SettleBill> settleBills = settleBillConverter.params2pos(settleBillParams);
        //dao批量写入
        //insert into settle_bille values (师傅账单),(平台账单)
        settleBillDAO.batchInsert(settleBills);
    }

    @Override
    public Long getSettleBillByOrderNo(String orderNo) {
        List<SettleBill> settleBills = settleBillDAO.getSettleBillByOrderNo(orderNo);
        //根据orderNo 查询 所有账单 最多2个 最少0个
        if (CollectionUtils.isEmpty(settleBills)) {
            return 0L;
        }
        return settleBills.size() + 0L;
    }

    @Override
    public void updateStatus(String orderNo, Integer status) {

    }


}
