/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.server.protocol.param;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SettleBillParam {
     //1
     String orderNo;
     //2
     Long orderGmtCreate;
     //1
     Long userId;
     //2
     Long paymentTime;
     //1
     String requestOrderNo;
     //1
     String requestOrderCategoryName;
     //1
     Long requestOrderPrice;
     //1 NULL
     Long requestOrderRawPrice;
     //2
     Long requestUserId;
     //2
     String requestUserName;
     //2
     String requestUserPhone;
     //1
     Long workerId;
     //1
     String workerName;
     //1
     String workerPhone;

     //profiteScale 2
     Long scale;
     //2
     Integer type;
     //2
     Long amount;
     //2
     Long totalAmount;
     //2
     Integer status;

}