/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.controller;

import com.tarena.tp.luban.settle.admin.assemble.SettleBillAssemble;
import com.tarena.tp.luban.settle.admin.bo.SettleBillBO;
import com.tarena.tp.luban.settle.admin.manager.SettleBillService;
import com.tarena.tp.luban.settle.admin.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.admin.protocol.query.SettleBillQuery;
import com.tarena.tp.luban.settle.admin.protocol.vo.SettleBillVO;
import com.tarena.tp.luban.settle.common.enums.SettleTypeEnum;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "settle",tags = "结算流水")
@RestController
@RequestMapping("admin/settle/bill")
public class SettleBillController {
    @Autowired
    private SettleBillService settleBillService;

    @Autowired
    private SettleBillAssemble settleBillAssemble;

    @PostMapping(value = "platform/search", produces = "application/json")
    @ResponseBody
    @ApiOperation("平台分润流水列表")
    public PagerResult<SettleBillVO> queryPlatformSettleBills(@RequestBody SettleBillQuery settleBillQuery) {
        settleBillQuery.setType(SettleTypeEnum.SETTLE_TYPE_PLATFORM.getStatus());
        ListRecordTotalBO<SettleBillBO> settleBillListTotalRecord = this.settleBillService.querySettleBill(settleBillQuery);
        return this.settleBillAssemble.assemblePagerResult(settleBillListTotalRecord, settleBillQuery);
    }

    @PostMapping(value = "worker/search", produces = "application/json")
    @ResponseBody
    @ApiOperation("师傅分润流水列表")
    public PagerResult<SettleBillVO> queryWorkerSettleBills(@RequestBody SettleBillQuery settleBillQuery) {
        settleBillQuery.setType(SettleTypeEnum.SETTLE_TYPE_WORKER.getStatus());
        ListRecordTotalBO<SettleBillBO> settleBillListTotalRecord = this.settleBillService.querySettleBill(settleBillQuery);
        return this.settleBillAssemble.assemblePagerResult(settleBillListTotalRecord, settleBillQuery);
    }


}