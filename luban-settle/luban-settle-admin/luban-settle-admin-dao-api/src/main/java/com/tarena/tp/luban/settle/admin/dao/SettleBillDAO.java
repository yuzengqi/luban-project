/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.dao;

import com.tarena.tp.luban.settle.po.SettleBill;
import com.tarena.tp.luban.settle.admin.dao.query.SettleBillDBPagerQuery;
import com.tedu.inn.protocol.dao.StatusCriteria;

import java.util.List;

public interface SettleBillDAO {
    Long insert(SettleBill settleBill);

    Integer delete(Long settleBillId);

    Integer update(SettleBill settleBill);

    SettleBill getEntity(Long settleBillId);

    Integer changeStatus(StatusCriteria statusCriteria);


    List<SettleBill> querySettleBills(SettleBillDBPagerQuery settleBillPagerQuery);

    Long countSettleBill(SettleBillDBPagerQuery settleBillPagerQuery);

}