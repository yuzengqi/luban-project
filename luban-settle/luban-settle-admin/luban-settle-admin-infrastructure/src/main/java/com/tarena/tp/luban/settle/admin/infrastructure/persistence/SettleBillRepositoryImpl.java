/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.infrastructure.persistence;

import com.tarena.tp.luban.settle.admin.dao.SettleBillDAO;
import com.tedu.inn.protocol.enums.StatusRecord;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.settle.admin.infrastructure.persistence.data.converter.SettleBillConverter;
import com.tarena.tp.luban.settle.po.SettleBill;
import com.tarena.tp.luban.settle.admin.bo.SettleBillBO;
import com.tarena.tp.luban.settle.admin.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.admin.repository.SettleBillRepository;
import com.tarena.tp.luban.settle.admin.protocol.query.SettleBillQuery;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SettleBillRepositoryImpl implements SettleBillRepository {
    @Autowired
    private SettleBillConverter settleBillConverter;

    @Autowired
    private SettleBillDAO settleBillDao;

    @Override public Long save(SettleBillParam settleBillParam) {
        SettleBill settleBill = this.settleBillConverter.param2po(settleBillParam);
        if (settleBill.getId() != null) {
            this.settleBillDao.update(settleBill);
            return settleBill.getId();
        }
        this.settleBillDao.insert(settleBill);
        return  settleBill.getId();
    }

    @Override public Integer delete(Long settleBillId) {
        return this.settleBillDao.delete(settleBillId);
    }

    @Override public Integer disable(String settleBillIds) {
        StatusCriteria statusCriteria = new StatusCriteria(settleBillIds, StatusRecord.DISABLE.getStatus());
        this.settleBillConverter.convertStatus(statusCriteria);
        return this.settleBillDao.changeStatus(statusCriteria);
    }

    @Override public Integer enable(String settleBillIds) {
        StatusCriteria statusCriteria = new StatusCriteria(settleBillIds, StatusRecord.ENABLE.getStatus());
        this.settleBillConverter.convertStatus(statusCriteria);
        return this.settleBillDao.changeStatus(statusCriteria);
    }

    @Override public SettleBillBO getSettleBill(Long settleBillId) {
        SettleBill settleBill = this.settleBillDao.getEntity(settleBillId);
        return this.settleBillConverter.po2bo(settleBill);
    }

    @Override public List<SettleBillBO> querySettleBills(
        SettleBillQuery settleBillQuery) {
        List<SettleBill> settleBillList = this.settleBillDao.querySettleBills(this.settleBillConverter.toDbPagerQuery(settleBillQuery));
        return this.settleBillConverter.poList2BoList(settleBillList);
    }

    @Override public Long getSettleBillCount(SettleBillQuery settleBillQuery) {
        return this.settleBillDao.countSettleBill(this.settleBillConverter.toDbPagerQuery(settleBillQuery));
    }
}