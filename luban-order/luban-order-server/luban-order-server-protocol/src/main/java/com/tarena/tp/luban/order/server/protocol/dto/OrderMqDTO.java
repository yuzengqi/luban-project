/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.order.server.protocol.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;


@Data
@ToString
public class OrderMqDTO implements Serializable {

    Long id;
    //1
    String orderNo;
    //1
    Long userId;
    //requestUserName
    String userName;
    //requestUserPhone
    String userPhone;

    String userAddress;
    //1
    Long workerId;
    //1
    String workerName;
    //1
    String workerPhone;
    //1
    String requestOrderNo;
    //1
    String requestOrderCategoryName;
    //1
    Long requestOrderPrice;
    //1 null
    Long requestOrderRawPrice;

    Long profitScale;

    Integer status;
    //orderGmtCreate
    Long gmtCreate;

    Long gmtModified;

    Long serviceTime;

    Long signTime;

    Long finishTime;

}
