package com.tarena.tp.luban.order.server.protocol.query;

import com.tedu.inn.protocol.pager.SimplePagerQuery;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderQuery extends SimplePagerQuery {

    List<Integer> status;
    //前端没有传递 后端根据jwt解析补充
    Long userId;
}
