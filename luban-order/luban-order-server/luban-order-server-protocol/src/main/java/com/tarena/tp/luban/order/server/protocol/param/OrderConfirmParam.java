package com.tarena.tp.luban.order.server.protocol.param;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderConfirmParam {

    String orderNo;

    List<Long> attachIds;

}
