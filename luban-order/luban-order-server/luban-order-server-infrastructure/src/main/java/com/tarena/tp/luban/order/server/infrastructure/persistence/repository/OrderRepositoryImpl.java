package com.tarena.tp.luban.order.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.order.po.Order;
import com.tarena.tp.luban.order.server.dao.OrderDAO;
import com.tarena.tp.luban.order.server.dao.query.OrderDBPagerQuery;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderRepository;
import com.tarena.tp.luban.order.server.infrastructure.persistence.data.converter.OrderConverter;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderRepositoryImpl implements OrderRepository {
    @Autowired(required = false)
    private OrderDAO orderDAO;

    @Autowired
    private OrderConverter orderConverter;

    @Override
    public Long save(OrderParam orderParam) {
        Order order = orderConverter.param2po(orderParam);
        return orderDAO.insert(order);
    }

    @Override
    public Integer delete(Long orderId) {
        return null;
    }

    @Override
    public OrderBO getOrder(Long orderId) {
        return null;
    }

    @Override
    public OrderBO getOrderByOrderNo(String orderNo) {
        Order order = orderDAO.getOrderByOrderNo(orderNo);
        OrderBO orderBO = orderConverter.po2bo(order);
        return orderBO;
    }

    @Override
    public Long getOrderCount(OrderQuery orderQuery) {
        OrderDBPagerQuery orderDBPagerQuery = orderConverter.toDbPagerQuery(orderQuery);
        Long total = orderDAO.countOrder(orderDBPagerQuery);
        return total;
    }

    @Override
    public List<OrderBO> queryOrders(OrderQuery orderBOQuery) {
        OrderDBPagerQuery orderDBPagerQuery = orderConverter.toDbPagerQuery(orderBOQuery);
        List<Order> orders = orderDAO.queryOrders(orderDBPagerQuery);
        List<OrderBO> orderBOS = orderConverter.poList2BoList(orders);
        return orderBOS;
    }

    @Override
    public Integer disable(String bankIds) {
        return null;
    }

    @Override
    public Integer enable(String bankIds) {
        return null;
    }

    @Override
    public void sign(OrderParam orderParam) {
        Order order = orderConverter.param2po(orderParam);
        orderDAO.updateStatus(order);
    }

    @Override
    public void confirm(OrderParam orderParam) {
        Order order = orderConverter.param2po(orderParam);
        orderDAO.updateStatus(order);
    }

    @Override
    public int finish(OrderParam orderParam) {
        Order order = orderConverter.param2po(orderParam);
        return orderDAO.updateStatus(order);
    }

    @Override
    public void complete(OrderParam orderParam) {
        Order order = orderConverter.param2po(orderParam);
        orderDAO.updateStatus(order);
    }

    @Override
    public void cancel(OrderParam orderParam) {

    }
}
