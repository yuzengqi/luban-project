/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.server.web.adapter.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderVO {

    @ApiModelProperty("id")
    private
    Long id;

    @ApiModelProperty("订单号")
    private
    String orderNo;

    @ApiModelProperty("用户id")
    private
    Long userId;

    @ApiModelProperty("用户姓名")
    private
    String userName;

    @ApiModelProperty("用户电话")
    private
    String userPhone;

    @ApiModelProperty("用户地址")
    private
    String userAddress;

    @ApiModelProperty("需求单号")
    private
    String requestOrderNo;

    @ApiModelProperty("需求单类型")
    private
    String requestOrderCategoryName;

    @ApiModelProperty("单价")
    private
    Long requestOrderPrice;

    @ApiModelProperty("下单日期")
    private
    Long gmtCreate;

    @ApiModelProperty("状态")
    private
    Integer status;

}