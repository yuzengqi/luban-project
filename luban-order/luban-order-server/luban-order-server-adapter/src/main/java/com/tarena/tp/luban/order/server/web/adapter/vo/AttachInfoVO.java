package com.tarena.tp.luban.order.server.web.adapter.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AttachInfoVO {
    @ApiModelProperty("地址")
    private
    String url;

}
