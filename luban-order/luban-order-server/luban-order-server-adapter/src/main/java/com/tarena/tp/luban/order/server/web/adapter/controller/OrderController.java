package com.tarena.tp.luban.order.server.web.adapter.controller;

import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.service.OrderService;
import com.tarena.tp.luban.order.server.protocol.param.OrderConfirmParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderFinishParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderSignParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tarena.tp.luban.order.server.web.adapter.assemble.OrderAssemble;
import com.tarena.tp.luban.order.server.web.adapter.vo.OrderDetailVO;
import com.tarena.tp.luban.order.server.web.adapter.vo.OrderVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.model.Result;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderAssemble orderAssemble;

    @PostMapping("/order/create")
    public Result<String> create(@RequestBody OrderParam orderParam) throws BusinessException {
        String orderNo = orderService.create(orderParam);
        return new Result<>(orderNo);
    }

    @PostMapping("/order/list")
    public Result<PagerResult<OrderVO>> orderList(@RequestBody OrderQuery orderQuery) {
        ListRecordTotalBO<OrderBO> orderBOS = orderService.orderList(orderQuery);
        PagerResult<OrderVO> orderVOPagerResult = orderAssemble.assemblePagerResult(orderBOS, orderQuery);
        return new Result<>(orderVOPagerResult);
    }

    @GetMapping("/order/info")
    public Result<OrderDetailVO> orderInfo(String orderNo) throws BusinessException {
        OrderBO orderBO = orderService.orderDetail(orderNo);
        OrderDetailVO orderDetailVO = orderAssemble.assembleBO2DetailVO(orderBO);
        return new Result<>(orderDetailVO);
    }

    @PostMapping("/order/sign")
    public Result<String> orderSign(@RequestBody OrderSignParam orderSignParam) throws BusinessException {
        String orderNo = orderService.orderSign(orderSignParam);
        return new Result<>(orderNo);
    }

    @PostMapping("/order/confirm")
    public Result<String> orderConfirm(@RequestBody OrderConfirmParam orderConfirmParam) throws BusinessException {
        String orderNo = orderService.confirm(orderConfirmParam);
        return new Result<>(orderNo);
    }

    @PostMapping("/order/finish")
    public Result<String> orderFinish(@RequestBody OrderFinishParam orderFinishParam) throws BusinessException {
        String orderNo = orderService.finish(orderFinishParam);
        return new Result<>(orderNo);
    }
}
