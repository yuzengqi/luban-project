/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.server.web.adapter.assemble;

import com.tarena.tp.luban.order.server.domain.bo.AttachInfoBO;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.web.adapter.vo.*;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.pager.PagerResult;
import com.tedu.inn.protocol.pager.SimplePagerQuery;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderAssemble {
    public OrderVO assembleOrderVO(OrderBO orderBO) {
        OrderVO orderVO = new OrderVO();
        BeanUtils.copyProperties(orderBO, orderVO);
        return orderVO;
    }

    public List<OrderVO> assembleOrderVOList(List<OrderBO> orderBOList) {
        if (CollectionUtils.isEmpty(orderBOList)) {
            return null;
        }
        return orderBOList.stream().map(this::assembleOrderVO).collect(Collectors.toList());
    }

    public PagerResult<OrderVO> assemblePagerResult(ListRecordTotalBO<OrderBO> orderListTotalRecord,
                                                    SimplePagerQuery orderQuery) {
        List<OrderVO> orderVOList = this.boListAssembleVOList(orderListTotalRecord.getList());
        PagerResult<OrderVO> pagerResult = new PagerResult<>(orderQuery);
        pagerResult.setObjects(orderVOList);
        pagerResult.setTotal(orderListTotalRecord.getTotal());
        return pagerResult;
    }

    public List<OrderVO> boListAssembleVOList(List<OrderBO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<OrderVO> orderVOList = new ArrayList<>(list.size());
        for (OrderBO orderBo : list) {
            OrderVO orderVo = this.assembleBO2VO(orderBo);
            orderVOList.add(orderVo);
        }
        return orderVOList;
    }

    public OrderVO assembleBO2VO(OrderBO bo) {
        OrderVO order = new OrderVO();
        BeanUtils.copyProperties(bo, order);
        return order;
    }

    public OrderDetailVO assembleBO2DetailVO(OrderBO bo) {
        OrderDetailVO order = new OrderDetailVO();
        //订单信息
        order.setOrderInfo(assembleOrderInfoVO(bo));
        //需求单信息
        order.setRequestOrderInfo(assembleRequestOrderInfoVO(bo));
        //附件信息
        order.setAttachInfo(assembleAttachInfoVO(bo.getAttachInfoBO()));
        //用户信息
        order.setUserInfo(assembleUserInfoVO(bo));
        return order;
    }

    public OrderInfoVo assembleOrderInfoVO(OrderBO bo) {
        OrderInfoVo order = new OrderInfoVo();
        if (bo == null) {
            return order;
        }
        BeanUtils.copyProperties(bo, order);
        order.setCreateTime(bo.getGmtCreate());
        return order;
    }

    public RequestOrderInfoVO assembleRequestOrderInfoVO(OrderBO bo) {
        RequestOrderInfoVO info = new RequestOrderInfoVO();
        if (bo == null) {
            return info;
        }
        BeanUtils.copyProperties(bo, info);
        return info;
    }

    public UserInfoVO assembleUserInfoVO(OrderBO bo) {
        UserInfoVO info = new UserInfoVO();
        if (bo == null) {
            return info;
        }
        BeanUtils.copyProperties(bo, info);
        return info;
    }


    public AttachInfoVO assembleAttachVO(AttachInfoBO bo) {
        AttachInfoVO info = new AttachInfoVO();
        if (bo == null) {
            return info;
        }
        BeanUtils.copyProperties(bo, info);
        return info;
    }

    public List<AttachInfoVO> assembleAttachInfoVO(List<AttachInfoBO> attachInfoBOList) {
        if (CollectionUtils.isEmpty(attachInfoBOList)) {
            return Collections.emptyList();
        }
        List<AttachInfoVO> orderVOList = new ArrayList<>(attachInfoBOList.size());
        for (AttachInfoBO attachInfoBO : attachInfoBOList) {
            AttachInfoVO orderVo = this.assembleAttachVO(attachInfoBO);
            orderVOList.add(orderVo);
        }
        return orderVOList;
    }
}
