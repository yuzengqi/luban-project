package com.tarena.tp.luban.order.server.web.adapter.producer;

import com.tarena.tp.luban.order.server.domain.service.MessageService;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class MessageServiceImpl implements MessageService {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Override
    public void sendOrderFinishMessage(OrderMqDTO orderMq) {
        //准备一个消息对象
        Message<OrderMqDTO> message = MessageBuilder.withPayload(orderMq).build();
        rocketMQTemplate.asyncSend("order_finish_topic", message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("发送成功");
            }

            @Override
            public void onException(Throwable throwable) {
                System.out.println("发送失败");
            }
        });
    }

    @Override
    public void sendOrderCancelMessage(OrderMqDTO orderMqDTO) {

    }

    @Override
    public void sendDelaySignMessage(String orderNo) {

    }
}
