/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.server.dao;

import com.tarena.tp.luban.order.po.Order;
import com.tarena.tp.luban.order.server.dao.query.OrderDBPagerQuery;
import com.tedu.inn.protocol.dao.StatusCriteria;

import java.util.List;

public interface OrderDAO {
    Long insert(Order order);

    Integer delete(Long orderId);

    Integer update(Order order);

    Order getEntity(Long orderId);

    Integer changeStatus(StatusCriteria statusCriteria);


    List<Order> queryOrders(OrderDBPagerQuery orderPagerQuery);

    Long countOrder(OrderDBPagerQuery orderPagerQuery);

    Order getOrderByOrderNo(String orderNo);

    int updateStatus(Order orderParam);
}