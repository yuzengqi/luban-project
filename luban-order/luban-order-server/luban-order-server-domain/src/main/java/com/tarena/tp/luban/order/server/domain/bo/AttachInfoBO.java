package com.tarena.tp.luban.order.server.domain.bo;


import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AttachInfoBO {

    String url;

}
