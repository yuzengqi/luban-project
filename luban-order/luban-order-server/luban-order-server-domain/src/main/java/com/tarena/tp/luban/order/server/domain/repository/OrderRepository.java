/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.server.domain.repository;

import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.bo.OrderLogBO;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;

import java.util.List;

public interface OrderRepository {
    Long save(OrderParam orderParam);

    Integer delete(Long orderId);

    OrderBO getOrder(Long orderId);

    OrderBO getOrderByOrderNo(String orderNo);

    Long getOrderCount(OrderQuery orderQuery);

    List<OrderBO> queryOrders(OrderQuery orderBOQuery);

    Integer disable(String bankIds);

    Integer enable(String bankIds);

    void sign(OrderParam orderParam);

    void confirm(OrderParam orderParam);

    int finish(OrderParam orderParam);

    void complete(OrderParam orderParam);

    void cancel(OrderParam orderParam);
}