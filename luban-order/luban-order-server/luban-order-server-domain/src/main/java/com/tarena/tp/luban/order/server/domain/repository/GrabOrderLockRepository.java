package com.tarena.tp.luban.order.server.domain.repository;

import com.tarena.tp.luban.order.server.protocol.param.OrderParam;

/**
 * 为师傅抢同一个需求单做的锁逻辑
 * redis实现 计步器
 */
public interface GrabOrderLockRepository {
    Boolean regrabLock(OrderParam orderParam);
}
