/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.order.server.domain.assembler;

import com.tarena.tp.luban.order.common.enums.OrderStatusEnum;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.order.server.protocol.param.OrderLogParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;


@Component
public class OrderAssembler {

    public OrderLogParam generateOrderLog(String orderNo, Long userId, OrderStatusEnum orderStatusEnum) {
        OrderLogParam logParam = new OrderLogParam();
        logParam.setOrderNo(orderNo);
        logParam.setUserId(userId);
        logParam.setOperateName(orderStatusEnum.getMessage());
        logParam.setRemark(orderStatusEnum.getMessage());
        long now = System.currentTimeMillis();
        logParam.setOperateTime(now);
        logParam.setGmtModified(now);
        logParam.setGmtCreate(now);
        return logParam;
    }

    public OrderMqDTO assembleOrderMqDTO(OrderBO orderBO) {
        OrderMqDTO orderMqDTO = new OrderMqDTO();
        if (orderBO == null) {
            return orderMqDTO;
        }
        orderMqDTO.setWorkerId(orderBO.getUserId());
        BeanUtils.copyProperties(orderBO, orderMqDTO);
        return orderMqDTO;
    }

    public OrderMqDTO assembleOrderMqDTO(OrderParam orderBO) {
        OrderMqDTO orderMqDTO = new OrderMqDTO();
        if (orderBO == null) {
            return orderMqDTO;
        }
        BeanUtils.copyProperties(orderBO, orderMqDTO);
        return orderMqDTO;
    }
}
