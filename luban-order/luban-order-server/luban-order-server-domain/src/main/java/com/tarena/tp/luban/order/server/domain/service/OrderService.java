package com.tarena.tp.luban.order.server.domain.service;

import com.github.xiaoymin.knife4j.core.util.CollectionUtils;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.param.AttachUpdateParam;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.demand.server.client.RequestOrderApi;
import com.tarena.tp.luban.order.common.enums.OrderStatusEnum;
import com.tarena.tp.luban.order.common.enums.ResultEnum;
import com.tarena.tp.luban.order.server.domain.assembler.OrderAssembler;
import com.tarena.tp.luban.order.server.domain.bo.AttachInfoBO;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderLogRepository;
import com.tarena.tp.luban.order.server.domain.repository.OrderRepository;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.order.server.protocol.param.*;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class OrderService {
    @Autowired(required = false)
    private RequestOrderApi requestOrderApi;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderLogRepository orderLogRepository;

    @Autowired
    private OrderAssembler orderAssembler;

    @Autowired(required = false)
    private AttachApi attachApi;

    @Autowired(required = false)
    private MessageService messageService;

    public String create(OrderParam orderParam) throws BusinessException {
        //要拿到需求单的抢单结果，然后抢到才能继续
        Boolean aBoolean = requestOrderApi.grabOrder(orderParam.getRequestOrderNo());
        if (!aBoolean) {
            throw new BusinessException(ResultEnum.ORDER_GRAB_FAIL);
        }
        //如果抢到，绑定当前师傅id
        Long userId = SecurityContext.getLoginToken().getUserId();
        String ordNo = UUID.randomUUID().toString();
        OrderStatusEnum orderCreateSuccess = OrderStatusEnum.ORDER_CREATE_SUCCESS;
        orderParam.setUserId(userId);
        orderParam.setOrderNo(ordNo);
        orderParam.setStatus(orderCreateSuccess.getStatus());
        //保存订单
        orderRepository.save(orderParam);
        //还要记录当前订单状态
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(ordNo, userId, orderCreateSuccess);
        orderLogRepository.save(orderLogParam);
        return ordNo;
    }

    public ListRecordTotalBO<OrderBO> orderList(OrderQuery orderQuery) {
        //获取userId
        Long userId = SecurityContext.getLoginToken().getUserId();
        orderQuery.setUserId(userId);
        //查询总条数
        Long total = orderRepository.getOrderCount(orderQuery);
        //查询记录
        List<OrderBO> orderBOS = null;
        if (total > 0) {
            orderBOS = orderRepository.queryOrders(orderQuery);
        }
        ListRecordTotalBO<OrderBO> listRecordTotalBOS = new ListRecordTotalBO<>(orderBOS, total);
        return listRecordTotalBOS;
    }

    public OrderBO orderDetail(String orderNo) throws BusinessException {
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        if (orderBO == null || orderBO.getUserId() == null) {
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //查询图片
        AttachQuery attachQuery = new AttachQuery();
        attachQuery.setBusinessType(200);
        attachQuery.setBusinessId(orderBO.getId().intValue());
        List<AttachDTO> attachDTOS = attachApi.getAttachInfoByParam(attachQuery);
        if (CollectionUtils.isEmpty(attachDTOS)) {
            orderBO.setAttachInfoBO(null);
        } else {
            String prefixPath = "http://localhost:8092/static/";
            List<AttachInfoBO> attachInfoBOS = new ArrayList<>();
            for (AttachDTO attachDTO : attachDTOS) {
                AttachInfoBO attachInfoBO = new AttachInfoBO();
                attachInfoBO.setUrl(prefixPath + attachDTO.getFileUuid());
                attachInfoBOS.add(attachInfoBO);
            }
            orderBO.setAttachInfoBO(attachInfoBOS);
        }
        return orderBO;
    }

    public String orderSign(OrderSignParam orderSignParam) throws BusinessException {
        //先查询订单，然后才有订单状态
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderSignParam.getOrderNo());
        String orderNo = orderBO.getOrderNo();
        if (orderBO == null || orderBO.getUserId() == null) {
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //然后看订单状态
        if (orderBO.getStatus() != 10) {
            throw new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR);
        }
        OrderParam orderParam = new OrderParam();
        orderParam.setStatus(20);
        orderParam.setOrderNo(orderNo);
        orderRepository.sign(orderParam);
        //还要记录日志
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(orderNo, SecurityContext.getLoginToken().getUserId(), OrderStatusEnum.ORDER_SIGN_SUCCESS);
        orderLogRepository.save(orderLogParam);
        return orderNo;
    }

    public String confirm(OrderConfirmParam orderConfirmParam) throws BusinessException {
        //查询订单
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderConfirmParam.getOrderNo());
        //判断订单
        if (orderBO == null || orderBO.getUserId() == null) {
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //判断状态
        if (orderBO.getStatus() != 20 || orderBO.getStatus() != 50) {
            throw new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR);
        }
        //绑定图片
        List<AttachUpdateParam> attachUpdateParams = new ArrayList<>();
        List<Long> attachIds = orderConfirmParam.getAttachIds();
        if (CollectionUtils.isNotEmpty(attachIds)) {
            for (Long attachId : attachIds) {
                AttachUpdateParam attachUpdateParam = new AttachUpdateParam();
                attachUpdateParam.setId(attachId.intValue());
                attachUpdateParam.setIsCover(0);
                attachUpdateParam.setBusinessType(200);
                attachUpdateParam.setBusinessId(orderBO.getId().intValue());
                attachUpdateParams.add(attachUpdateParam);
            }
            attachApi.batchUpdateAttachByIdList(attachUpdateParams);
        } else {
            throw new BusinessException(ResultEnum.ATTACH_CONFIRM_ERROR);
        }
        //更改状态
        if (orderBO.getStatus() != 50) {
            OrderParam orderParam = new OrderParam();
            orderParam.setOrderNo(orderConfirmParam.getOrderNo());
            orderParam.setStatus(50);
            orderRepository.confirm(orderParam);
        }
        //记录日志
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(orderConfirmParam.getOrderNo(), SecurityContext.getLoginToken().getUserId(), OrderStatusEnum.ORDER_ATTACH_CONFIRM);
        orderLogRepository.save(orderLogParam);
        return orderConfirmParam.getOrderNo();
    }

    public String finish(OrderFinishParam orderFinishParam) throws BusinessException {
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderFinishParam.getOrderNo());
        if (orderBO == null || orderBO.getUserId() == null) {
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        if (orderBO.getStatus() != 50) {
            throw new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR);
        }
        OrderParam orderParam = new OrderParam();
        orderParam.setOrderNo(orderBO.getOrderNo());
        //订单完成状态
        orderParam.setStatus(OrderStatusEnum.ORDER_SERVICE_SUCCESS_UN_SETTLE.getStatus());
        orderRepository.finish(orderParam);
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(orderFinishParam.getOrderNo(), SecurityContext.getLoginToken().getUserId(), OrderStatusEnum.ORDER_SERVICE_SUCCESS_UN_SETTLE);
        orderLogRepository.save(orderLogParam);
        OrderMqDTO orderMqDTO = orderAssembler.assembleOrderMqDTO(orderBO);
        messageService.sendOrderFinishMessage(orderMqDTO);
        return orderFinishParam.getOrderNo();
    }

    public void complete(String s) {
        OrderBO orderBO = orderRepository.getOrderByOrderNo(s);
        if (orderBO == null || orderBO.getId() == null) {
            System.out.println("当前订单没有数据,orderNo为: " + s);
            return;
        }
        if (orderBO.getStatus() != 30) {
            System.out.println("订单流转状态不正确,orderNo为: " + s);
            return;
        }
        OrderParam orderParam = new OrderParam();
        orderParam.setOrderNo(s);
        orderParam.setStatus(OrderStatusEnum.ORDER_FINISH.getStatus());
        orderRepository.complete(orderParam);
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(s, 999L, OrderStatusEnum.ORDER_FINISH);
        orderLogRepository.save(orderLogParam);
    }
}
