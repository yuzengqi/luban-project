/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.order.common.enums;

import com.tedu.inn.protocol.enums.ErrorSupport;
import lombok.Getter;
@Getter
public enum ResultEnum implements ErrorSupport {

    BIND_ERROR("1", "非法入参"),

    GOODS_NUMS_ERROR("1000", "商品不存在"),

    ORDER_EMPTY("2000", "订单不存在"),

    ORDER_GRAB_FAIL("2003", "抢单失败"),

    ORDER_RE_GRAB("2004", "抢单操作太频繁,请稍后重试"),

    ORDER_UPDATE_ERROR("2001", "订单更新失败"),

    ORDER_GOODS_EMPTY("2002", "需求单不存在"),

    PRICE_ERROR("3000", "计价错误"),

    STOCK_ERROR("4000", "抢单失败"),

    SETTLE_ORDER_NO_EMPTY("5000", "结算回调,订单号为空"),

    UN_SETTLE_ORDER_ERROR("5001", "订单非结算状态"),

    SETTLE_CALL_ORDER_STATUS_ERROR("5002", "订单结算回调,订单状态非待结算"),

    USER_ID_EMPTY("6000", "userId丢失"),

    ORDER_STATUS_TRANSFORM_ERROR("6000", "订单状态流转错误"),

    USER_TOKEN_VERIFY_FAILED("6666", "用户信息验证失败"),

    SYSTEM_ERROR("-1", "系统异常"),

    ATTACH_CONFIRM_ERROR("10000","绑定图片异常" );

    ResultEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;

    private String message;

}
