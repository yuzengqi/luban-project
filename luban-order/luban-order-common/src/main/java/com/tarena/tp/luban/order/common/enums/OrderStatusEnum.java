/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.order.common.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * 订单状态枚举
 */
@Getter
public enum OrderStatusEnum {


    ORDER_CREATE_SUCCESS(10, "抢单成功"),

    ORDER_SIGN_SUCCESS(20, "签到"),

    ORDER_SERVICE_SUCCESS_UN_SETTLE(30, "服务完成待结算"),

    ORDER_FINISH(40, "完成订单"),

    ORDER_ATTACH_CONFIRM(50,"图片绑定"),

    CANCELLED_BY_USER(-10, "用户关闭"),

    CANCELLED_BY_PLATFORM(-20, "平台关闭(商家)"),

    CANCELLED_BY_EXPIRE(-30, "超时关闭"),
    ;

    OrderStatusEnum(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    private Integer status;

    private String message;

    public static List<Integer> cancelStatus() {
        return Arrays.asList(CANCELLED_BY_USER.getStatus(), CANCELLED_BY_PLATFORM.getStatus(), CANCELLED_BY_EXPIRE.getStatus());
    }

    public static Boolean isSignStatus(Integer status){
        return ORDER_SIGN_SUCCESS.getStatus().equals(status);
    }

    public static Boolean isFinishStatus(Integer status){
        return ORDER_FINISH.getStatus().equals(status);
    }

    public static Boolean isServiceSuccessStatus(Integer status){
        return ORDER_SERVICE_SUCCESS_UN_SETTLE.getStatus().equals(status);
    }

    public static Boolean isCreateSuccessStatus(Integer status){
        return ORDER_CREATE_SUCCESS.getStatus().equals(status);
    }

    public static Boolean isErrorStatus(Integer source, Integer target){
        boolean create2Sign = OrderStatusEnum.isCreateSuccessStatus(source) && OrderStatusEnum.isSignStatus(target);
        boolean sign2ServiceSuccess = OrderStatusEnum.isSignStatus(source) && OrderStatusEnum.isServiceSuccessStatus(target);
        boolean sign2Finish = OrderStatusEnum.isServiceSuccessStatus(source) && OrderStatusEnum.isFinishStatus(target);
        return  !(create2Sign || sign2Finish || sign2ServiceSuccess);
    }
}
