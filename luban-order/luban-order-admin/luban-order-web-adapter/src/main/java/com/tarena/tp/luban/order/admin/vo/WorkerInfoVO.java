package com.tarena.tp.luban.order.admin.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkerInfoVO {

    @ApiModelProperty("师傅姓名")
    String workerName;

    @ApiModelProperty("用户电话")
    String workerPhone;

}
