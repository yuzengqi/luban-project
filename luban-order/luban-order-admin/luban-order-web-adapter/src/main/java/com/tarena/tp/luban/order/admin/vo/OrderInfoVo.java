package com.tarena.tp.luban.order.admin.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderInfoVo {

    @ApiModelProperty("订单号")
    String orderNo;

    @ApiModelProperty("订单状态")
    Integer status;

    @ApiModelProperty("下单日期")
    Long gmtCreate;

    @ApiModelProperty("订单完成日期")
    Long gmtFinish;
}
