/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.admin.controller;

import com.tarena.tp.luban.order.admin.assemble.OrderAssemble;
import com.tarena.tp.luban.order.admin.bo.OrderBO;
import com.tarena.tp.luban.order.admin.manager.OrderService;
import com.tarena.tp.luban.order.admin.protocol.param.OrderParam;
import com.tarena.tp.luban.order.admin.protocol.query.OrderQuery;
import com.tarena.tp.luban.order.admin.vo.OrderDetailVO;
import com.tarena.tp.luban.order.admin.vo.OrderVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "order",tags = "订单接口")
@RestController
@RequestMapping("admin/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderAssemble orderAssemble;

    @PostMapping(value = "search", produces = "application/json")
    @ResponseBody
    @ApiOperation("订单列表")
    public PagerResult<OrderVO> queryOrders(@RequestBody OrderQuery orderQuery) {
        ListRecordTotalBO<OrderBO> orderListTotalRecord = this.orderService.queryOrder(orderQuery);
        return this.orderAssemble.assemblePagerResult(orderListTotalRecord, orderQuery);
    }

    @GetMapping(value = "get", produces = "application/json")
    @ApiOperation("订单详情")
    public OrderDetailVO getOrder(Long id)  {
        OrderBO orderBo = orderService.getOrder(id);
        return this.orderAssemble.assembleBO2DetailVO(orderBo);
    }

}