package com.tarena.tp.luban.order.admin.bo;


import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderLogBO {

    Long operateTime;

    String operateName;

    String remark;

    String orderNo;

}
