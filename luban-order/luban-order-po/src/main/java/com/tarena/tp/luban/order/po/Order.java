/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.po;

import com.tedu.inn.protocol.dao.PO;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Table(name = "order")
public class Order extends PO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)")
    /**
     * 主键
     */
    private Long id;

    /**
     * 订单编号
     */
    @Column(name = "order_no", columnDefinition = "varchar(16) DEFAULT '' COMMENT '订单编号'")
    private String orderNo;

    /**
     * 用户id
     */
    @Column(name = "user_id", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '用户id'", updatable = false)
    private Long userId;

    @Column(name = "user_name", columnDefinition = "varchar(64)  DEFAULT '' COMMENT '用户姓名'", updatable = false)
    private String userName;


    @Column(name = "user_phone", columnDefinition = "varchar(64)  DEFAULT '' COMMENT '用户电话'", updatable = false)
    private String userPhone;


    @Column(name = "user_address", columnDefinition = "varchar(64)  DEFAULT '' COMMENT '用户地址'", updatable = false)
    private String userAddress;

    @Column(name = "worker_name", columnDefinition = "varchar(64)  DEFAULT '' COMMENT '师傅姓名'", updatable = false)
    private String workerName;


    @Column(name = "worker_phone", columnDefinition = "varchar(16)  DEFAULT '' COMMENT '师傅电话'", updatable = false)
    private String workerPhone;

    /**
     * 需求单编号
     */
    @Column(name = "request_order_no", columnDefinition = "varchar(16) DEFAULT '' COMMENT '需求单编号'")
    private String requestOrderNo;

    /**
     * 需求单类型
     */
    @Column(name = "request_category_name", columnDefinition = "varchar(16) DEFAULT '' COMMENT '订单类型名称'")
    private String orderCategoryName;


    @Column(name = "request_category_id", columnDefinition = "int(16) DEFAULT 0 COMMENT '订单类型id'")
    private Integer orderCategoryId;

    /**
     * 需求单价格
     */
    @Column(name = "request_order_price", columnDefinition = "int(16) DEFAULT 0 COMMENT '需求单编号'")
    private Long requestOrderPrice;

    /**
     * 需求单价格
     */
    @Column(name = "request_order_raw_price", columnDefinition = "int(16) DEFAULT 0 COMMENT '原始价格'")
    private Long requestOrderRawPrice;

    /**
     * 分润比例
     */
    @Column(name = "profit_scale", columnDefinition = "int(16) DEFAULT 0 COMMENT '分润比例'")
    private Long profitScale;

    /**
     * 是否有效状态
     */
    @Column(name = "status", columnDefinition = "tinyint(1)  DEFAULT 0 COMMENT '状态 0:无效，1:有效'")
    private Integer status;

    @Column(
            name = "service_time",
            columnDefinition = "bigint(11)  DEFAULT 0 COMMENT '服务时间'",
            nullable = false
    )
    private Long serviceTime;
}
