/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.20 : Database - tarena_tp_luban_order
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tarena_tp_luban_order` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tarena_tp_luban_order`;

/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(64) DEFAULT '' COMMENT '订单编号',
  `user_id` int(11) unsigned DEFAULT '0' COMMENT '用户id',
  `user_name` varchar(64) DEFAULT '' COMMENT '用户姓名',
  `user_phone` varchar(16) DEFAULT '' COMMENT '用户电话',
  `user_address` varchar(64) DEFAULT '' COMMENT '用户地址',
  `worker_name` varchar(64) DEFAULT '' COMMENT '师傅姓名',
  `worker_phone` varchar(16) DEFAULT '' COMMENT '师傅电话',
  `request_order_no` varchar(64) DEFAULT '' COMMENT '需求单编号',
  `order_category_id` int(16) DEFAULT '0' COMMENT '订单类型id',
  `order_category_name` varchar(16) DEFAULT '' COMMENT '需求单类型',
  `request_order_price` int(11) DEFAULT '0' COMMENT '需求单价格',
  `request_order_raw_price` int(16) DEFAULT '0' COMMENT '原始价格',
  `profit_scale` int(16) DEFAULT '0' COMMENT '分润比例',
  `service_time` bigint(11) DEFAULT '0' COMMENT '服务时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0:无效，1:有效',
  `create_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `create_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `modified_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `modified_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `gmt_modified` bigint(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `gmt_create` bigint(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COMMENT='order';

/*Data for the table `order` */

insert  into `order`(`id`,`order_no`,`user_id`,`user_name`,`user_phone`,`user_address`,`worker_name`,`worker_phone`,`request_order_no`,`order_category_id`,`order_category_name`,`request_order_price`,`request_order_raw_price`,`profit_scale`,`service_time`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (41,'837120879920000011',21,'王五','13012345678','北京市海淀区中鼎大厦7层','代用名','13812353456','123123123',2,'厨具安装',80,100,20,1681897784819,40,'mock',0,0,'mock',1683712972071,1683712088009);
insert  into `order`(`id`,`order_no`,`user_id`,`user_name`,`user_phone`,`user_address`,`worker_name`,`worker_phone`,`request_order_no`,`order_category_id`,`order_category_name`,`request_order_price`,`request_order_raw_price`,`profit_scale`,`service_time`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (42,'837128358730010011',21,'张三','13266668888','北京市海淀区中鼎大厦7层','代用名','13812353456','32232964103521',1,'衣柜安装',80,100,20,1681897784819,40,'mock',0,0,'mock',1683713565287,1683712835878);
insert  into `order`(`id`,`order_no`,`user_id`,`user_name`,`user_phone`,`user_address`,`worker_name`,`worker_phone`,`request_order_no`,`order_category_id`,`order_category_name`,`request_order_price`,`request_order_raw_price`,`profit_scale`,`service_time`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (43,'837929496700020011',21,'张三','13266668888','北京市海淀区中鼎大厦7层','代用名','13812353456','32232964103522',1,'衣柜安装',80,100,20,1681897784819,10,'test004',21,21,'test004',1683792949675,1683792949675);
insert  into `order`(`id`,`order_no`,`user_id`,`user_name`,`user_phone`,`user_address`,`worker_name`,`worker_phone`,`request_order_no`,`order_category_id`,`order_category_name`,`request_order_price`,`request_order_raw_price`,`profit_scale`,`service_time`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (44,'837931686790030011',21,'王五','13012345678','北京市海淀区中鼎大厦7层','代用名','13812353456','123123123',2,'厨具安装',80,100,20,1681897784819,30,'test004',21,21,'test004',1686035696500,1683793168684);
insert  into `order`(`id`,`order_no`,`user_id`,`user_name`,`user_phone`,`user_address`,`worker_name`,`worker_phone`,`request_order_no`,`order_category_id`,`order_category_name`,`request_order_price`,`request_order_raw_price`,`profit_scale`,`service_time`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (45,'855841350980040011',21,'张三','13266668888','北京市海淀区中鼎大厦7层','代用名','13812353456','32232964103521',1,'衣柜安装',80,100,20,1681897784819,30,'test004',21,21,'test004',1685584169544,1685584135103);

/*Table structure for table `order_log` */

DROP TABLE IF EXISTS `order_log`;

CREATE TABLE `order_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT '用户id',
  `order_no` varchar(64) DEFAULT '' COMMENT '订单编号',
  `operate_time` bigint(20) DEFAULT '0' COMMENT '操作时间',
  `operate_name` varchar(16) DEFAULT '' COMMENT '操作步骤',
  `remark` varchar(16) DEFAULT '' COMMENT '备注',
  `create_user_name` varchar(16) DEFAULT '' COMMENT '创建人',
  `create_user_id` int(10) unsigned DEFAULT '0' COMMENT '创建人id',
  `modified_user_id` int(10) unsigned DEFAULT '0' COMMENT '修改人id',
  `modified_user_name` varchar(16) DEFAULT '' COMMENT '修改人',
  `gmt_create` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `gmt_modified` bigint(20) DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0:无效，1:有效',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8mb4 COMMENT='order_log';

/*Data for the table `order_log` */

insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (120,21,'837120879920000011',1683712088004,'抢单成功','抢单成功','test004',21,21,'test004',1683712088026,1683712088026,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (121,21,'837120879920000011',1683712191601,'签到','签到','test004',21,21,'test004',1683712191607,1683712191607,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (122,21,'837120879920000011',1683712215045,'服务完成待结算','服务完成待结算','test004',21,21,'test004',1683712215050,1683712215050,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (123,21,'837128358730010011',1683712835878,'抢单成功','抢单成功','test004',21,21,'test004',1683712835884,1683712835884,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (124,21,'837120879920000011',1683712972071,'完成订单','完成订单','mock',0,0,'mock',1683712972077,1683712972077,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (125,21,'837128358730010011',1683713522841,'签到','签到','test004',21,21,'test004',1683713522846,1683713522846,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (126,21,'837128358730010011',1683713559846,'服务完成待结算','服务完成待结算','test004',21,21,'test004',1683713559851,1683713559851,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (127,21,'837128358730010011',1683713565287,'完成订单','完成订单','mock',0,0,'mock',1683713565293,1683713565293,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (128,21,'837929496700020011',1683792949675,'抢单成功','抢单成功','test004',21,21,'test004',1683792949682,1683792949682,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (129,21,'837931686790030011',1683793168684,'抢单成功','抢单成功','test004',21,21,'test004',1683793168690,1683793168690,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (130,21,'837931686790030011',1684137616343,'签到','签到','test004',21,21,'test004',1684137616348,1684137616348,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (131,21,'855841350980040011',1685584135103,'抢单成功','抢单成功','test004',21,21,'test004',1685584135110,1685584135110,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (132,21,'855841350980040011',1685584153614,'签到','签到','test004',21,21,'test004',1685584153619,1685584153619,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (133,21,'855841350980040011',1685584169544,'服务完成待结算','服务完成待结算','test004',21,21,'test004',1685584169549,1685584169549,1);
insert  into `order_log`(`id`,`user_id`,`order_no`,`operate_time`,`operate_name`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`,`status`) values (134,21,'837931686790030011',1686035696498,'服务完成待结算','服务完成待结算','test004',21,21,'test004',1686035696508,1686035696508,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
