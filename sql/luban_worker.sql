/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.20 : Database - tarena_tp_luban_worker
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tarena_tp_luban_worker` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tarena_tp_luban_worker`;

/*Table structure for table `worker` */

DROP TABLE IF EXISTS `worker`;

CREATE TABLE `worker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT 'Passport 用户ID',
  `birthday` datetime DEFAULT NULL COMMENT '出生日期',
  `phone` varchar(16) DEFAULT '' COMMENT '手机号',
  `real_name` varchar(16) DEFAULT '' COMMENT '真实姓名',
  `id_card` varchar(64) DEFAULT '' COMMENT '身份证号',
  `status` tinyint(1) DEFAULT '0' COMMENT '账号状态 0:锁定，1:正常',
  `create_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `create_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `modified_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `modified_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `gmt_modified` bigint(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `gmt_create` bigint(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `cert_status` tinyint(1) DEFAULT NULL COMMENT '认证的状态: 0:无效 1:通过',
  `audit_status` tinyint(4) DEFAULT NULL COMMENT '审核状态 0:驳回，1:通过  2:未审核',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COMMENT='worker';

/*Data for the table `worker` */

insert  into `worker`(`id`,`user_id`,`birthday`,`phone`,`real_name`,`id_card`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`,`cert_status`,`audit_status`) values (22,21,NULL,'13812353456','代用名','430512198908131367',1,'test004',21,0,'mock',1684737062120,1683708611930,1,1);

/*Table structure for table `worker_area` */

DROP TABLE IF EXISTS `worker_area`;

CREATE TABLE `worker_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT '用户ID',
  `area_id` bigint(20) DEFAULT '0' COMMENT '区域id',
  `area_detail` varchar(64) DEFAULT NULL COMMENT '区域详情',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0:无效，1:有效',
  `create_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `create_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `modified_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `modified_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `gmt_modified` bigint(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `gmt_create` bigint(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COMMENT='worker_area';

/*Data for the table `worker_area` */

insert  into `worker_area`(`id`,`user_id`,`area_id`,`area_detail`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (33,21,1151,'相山区',1,'test004',21,21,'test004',1683708611993,1683708611993);
insert  into `worker_area`(`id`,`user_id`,`area_id`,`area_detail`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (34,21,1152,'烈山区',1,'test004',21,21,'test004',1683708612000,1683708612000);
insert  into `worker_area`(`id`,`user_id`,`area_id`,`area_detail`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`) values (35,21,1153,'濉溪县',1,'test004',21,21,'test004',1683708612007,1683708612007);

/*Table structure for table `worker_audit_log` */

DROP TABLE IF EXISTS `worker_audit_log`;

CREATE TABLE `worker_audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT '审核人id',
  `user_name` varchar(16) DEFAULT '' COMMENT '审核人姓名',
  `worker_id` int(10) unsigned DEFAULT '0' COMMENT '师傅id',
  `operate_time` bigint(20) DEFAULT '0' COMMENT '操作时间',
  `operate_name` varchar(16) DEFAULT '' COMMENT '操作步骤',
  `audit_status` tinyint(4) DEFAULT '0' COMMENT '状态 0:驳回，1:通过,  2:待审核',
  `reject_reason` varchar(64) DEFAULT '' COMMENT '驳回原因',
  `remark` varchar(64) DEFAULT '' COMMENT '备注',
  `create_user_name` varchar(16) DEFAULT '' COMMENT '创建人',
  `create_user_id` int(10) unsigned DEFAULT '0' COMMENT '创建人id',
  `modified_user_id` int(10) unsigned DEFAULT '0' COMMENT '修改人id',
  `modified_user_name` varchar(16) DEFAULT '' COMMENT '修改人',
  `gmt_create` bigint(20) DEFAULT '0' COMMENT '创建时间',
  `gmt_modified` bigint(20) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COMMENT='师傅审核记录';

/*Data for the table `worker_audit_log` */

insert  into `worker_audit_log`(`id`,`user_id`,`user_name`,`worker_id`,`operate_time`,`operate_name`,`audit_status`,`reject_reason`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`) values (38,NULL,NULL,21,1683708655113,'mock',1,NULL,NULL,'mock',0,0,'mock',1683708655113,1683708655113);
insert  into `worker_audit_log`(`id`,`user_id`,`user_name`,`worker_id`,`operate_time`,`operate_name`,`audit_status`,`reject_reason`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`) values (39,NULL,NULL,21,1683708767966,'mock',1,NULL,NULL,'mock',0,0,'mock',1683708767966,1683708767966);
insert  into `worker_audit_log`(`id`,`user_id`,`user_name`,`worker_id`,`operate_time`,`operate_name`,`audit_status`,`reject_reason`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`) values (40,NULL,NULL,21,1683708905019,'mock',1,NULL,NULL,'mock',0,0,'mock',1683708905019,1683708905019);
insert  into `worker_audit_log`(`id`,`user_id`,`user_name`,`worker_id`,`operate_time`,`operate_name`,`audit_status`,`reject_reason`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`) values (41,NULL,NULL,21,1683709071491,'mock',1,NULL,NULL,'mock',0,0,'mock',1683709071491,1683709071491);
insert  into `worker_audit_log`(`id`,`user_id`,`user_name`,`worker_id`,`operate_time`,`operate_name`,`audit_status`,`reject_reason`,`remark`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_create`,`gmt_modified`) values (42,NULL,NULL,21,1683709386037,'mock',1,NULL,NULL,'mock',0,0,'mock',1683709386037,1683709386037);

/*Table structure for table `worker_category` */

DROP TABLE IF EXISTS `worker_category`;

CREATE TABLE `worker_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT '0' COMMENT '用户ID',
  `category_id` bigint(20) DEFAULT '0' COMMENT '品类id',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态 0:无效，1:有效',
  `create_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '创建人',
  `create_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建人ID',
  `modified_user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新人ID',
  `modified_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '更新人',
  `gmt_modified` bigint(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `gmt_create` bigint(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `category_detail` varchar(255) DEFAULT NULL COMMENT '品类详情',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COMMENT='worker_category';

/*Data for the table `worker_category` */

insert  into `worker_category`(`id`,`user_id`,`category_id`,`status`,`create_user_name`,`create_user_id`,`modified_user_id`,`modified_user_name`,`gmt_modified`,`gmt_create`,`category_detail`) values (18,21,4,1,'test004',21,21,'test004',1683708612022,1683708612022,'冰箱安装');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
