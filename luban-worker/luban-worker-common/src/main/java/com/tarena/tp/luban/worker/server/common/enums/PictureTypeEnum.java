package com.tarena.tp.luban.worker.server.common.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@Getter
public enum PictureTypeEnum {

    HEAD_PORTRAIT(1, "头像"),

    ID_CARD_NO(2,"身份证");


    PictureTypeEnum(Integer value, String message) {
        this.value = value;
        this.message = message;
    }

    public static boolean isValidEnum(Integer value) {
        return Arrays.stream(PictureTypeEnum.values()).anyMatch(o -> Objects.equals(o.getValue(), value));
    }

    private Integer value;

    private String message;
}
