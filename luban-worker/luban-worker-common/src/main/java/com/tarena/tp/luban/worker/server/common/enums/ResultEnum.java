/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.worker.server.common.enums;

import com.tedu.inn.protocol.enums.ErrorSupport;
import lombok.Getter;

@Getter
public enum ResultEnum implements ErrorSupport {

    BIND_ERROR("1", "非法入参"),

    ID_CARD_VERIFY_FAILED("1000", "身份信息验证失败"),

    ATTACH_FAILED("3000", "附件信息获取异常"),

    SYSTEM_ERROR("-1", "系统异常"),

    WORKER_NOT_FOUND("2000", "师傅不存在"),

    USER_TOKEN_VERIFY_FAILED("6666", "用户信息验证失败"),
    AUDIT_STATUS_TRANSFER_ERROR("40001", "审核提交状态流转失败"),
    ;


    ResultEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;

    private String message;


}
