package com.tarena.tp.luban.worker.server.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "baidu.card")
public class IdCardConfig {

    /**
     * url地址
     */
    private String url;

    /**
     * token
     */
    private String token;

}
