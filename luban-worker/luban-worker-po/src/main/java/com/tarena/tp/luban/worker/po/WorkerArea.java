/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.po;

import com.tedu.inn.protocol.dao.PO;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "worker_area")
public class WorkerArea extends PO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "int(11)")
    /**
     * 主键
     */
    private Long id;

    /**
     * 用户ID
     */
    @Column(name = "user_id", columnDefinition = "int(10) UNSIGNED DEFAULT 0 COMMENT '用户ID'", updatable = false)
    private Long userId;

    /**
     * 区域id
     */
    @Column(name = "area_id", columnDefinition = "bigint(20)  DEFAULT 0 COMMENT '区域id'")
    private Long areaId;

    /**
     * 是否有效状态
     */
    @Column(name = "status", columnDefinition = "tinyint(1)  DEFAULT 0 COMMENT '状态 0:无效，1:有效'")
    private Integer status;

    /**
     * 区域详情
     */
    @Column(name="area_detail",columnDefinition = "varchar(16) DEFAULT NULL COMMENT '区域详情'")
    private String areaDetail;
}
