package com.tarena.tp.luban.worker.server.web.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountVO {

    @ApiModelProperty(value = "金额")
    Long totalAmount;

}














