package com.tarena.tp.luban.worker.server.web.assemble;

import com.tarena.tp.luban.worker.server.bo.*;
import com.tarena.tp.luban.worker.server.web.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class WorkerAssemble {

    public WorkerVO assembleBO2VO(WorkerBO bo) {
        WorkerVO workerDetailVO = new WorkerVO();
        workerDetailVO.setAreaList(assembleAreaBO2VO(bo.getWorkerAreaBOList()));
        workerDetailVO.setCategoryList(assembleCategoryBO2VO(bo.getWorkerCategoryBOList()));
        workerDetailVO.setAttachList(assembleAttachBO2VO(bo.getAttachInfoBOList()));
        workerDetailVO.setAccountVO(assembleAccountBO2VO(bo.getAccountBO()));
        BeanUtils.copyProperties(bo, workerDetailVO);
        return workerDetailVO;
    }

    private AccountVO assembleAccountBO2VO(AccountBO bo) {
        AccountVO vo = new AccountVO();
        BeanUtils.copyProperties(bo, vo);
        return vo;
    }

    public List<AreaVO> assembleAreaBO2VO(List<WorkerAreaBO> workerAreaBOList) {
        if (CollectionUtils.isEmpty(workerAreaBOList)) {
            return Collections.emptyList();
        }
        List<AreaVO> result = new ArrayList<>();
        workerAreaBOList.forEach(workerAreaBO -> {
            result.add(assembleBO2VO(workerAreaBO));
        });
        return result;
    }


    public AreaVO assembleBO2VO(WorkerAreaBO bo) {
        AreaVO areaVO = new AreaVO();
        BeanUtils.copyProperties(bo, areaVO);
        return areaVO;
    }

    public CategoryVO assembleBO2VO(WorkerCategoryBO bo) {
        CategoryVO categoryVO = new CategoryVO();
        BeanUtils.copyProperties(bo, categoryVO);
        return categoryVO;
    }

    public List<CategoryVO> assembleCategoryBO2VO(List<WorkerCategoryBO> workerCategoryBOList) {
        if (CollectionUtils.isEmpty(workerCategoryBOList)) {
            return Collections.emptyList();
        }
        List<CategoryVO> result = new ArrayList<>();
        workerCategoryBOList.forEach(workerCategoryBO -> {
            result.add(assembleBO2VO(workerCategoryBO));
        });
        return result;
    }

    public List<String> assembleAttachBO2VO(List<AttachInfoBO> attachInfoBOList) {
        if (CollectionUtils.isEmpty(attachInfoBOList)) {
            return Collections.emptyList();
        }
        List<String> result = new ArrayList<>();
        attachInfoBOList.forEach(attachInfoBO -> {
            result.add(attachInfoBO.getUrl());
        });
        return result;
    }
}
