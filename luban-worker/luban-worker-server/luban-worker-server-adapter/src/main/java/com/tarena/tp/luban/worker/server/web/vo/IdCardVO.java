package com.tarena.tp.luban.worker.server.web.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class IdCardVO {
    @ApiModelProperty("是否合法")
    private boolean valid;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("身份证号")
    private String cardNo;

}
