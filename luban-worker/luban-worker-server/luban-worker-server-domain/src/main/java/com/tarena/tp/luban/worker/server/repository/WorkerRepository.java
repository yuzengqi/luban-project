package com.tarena.tp.luban.worker.server.repository;

import com.tarena.tp.luban.worker.server.bo.WorkerBO;
import com.tarena.tp.luban.worker.server.dto.param.WorkerCreateParam;
import com.tedu.inn.protocol.exception.BusinessException;

public interface WorkerRepository {


//    IdCardBO checkCard(List<IdCardParam> idCardParam) throws BusinessException;

    Long save(WorkerCreateParam workerCreateParam) throws  BusinessException;

    WorkerBO getWorker(Long workerId) throws  BusinessException;

    void delete(Long userId);
}
