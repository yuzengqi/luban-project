package com.tarena.tp.luban.worker.server.bo;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class IdCardBO {

     boolean valid;

     String realName;

     String cardNo;
    
}
