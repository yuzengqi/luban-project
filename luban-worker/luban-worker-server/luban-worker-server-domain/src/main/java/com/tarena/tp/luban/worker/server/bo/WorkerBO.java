/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.server.bo;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.util.Date;
import java.util.List;

/**
 * 基础信息:realName,phone,家庭地址
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
public class WorkerBO {
    Long id;
    Long userId;
    Date birthday;
    List<WorkerAreaBO> workerAreaBOList;
    List<WorkerCategoryBO> workerCategoryBOList;
    String phone;
    String realName;
    String IdCard;
    Integer status;
    Integer sex; //性别 1:男性  2:女性
    List<AttachInfoBO>  attachInfoBOList;
    Integer auditStatus;

    String rejectReason;

    AccountBO accountBO;
}