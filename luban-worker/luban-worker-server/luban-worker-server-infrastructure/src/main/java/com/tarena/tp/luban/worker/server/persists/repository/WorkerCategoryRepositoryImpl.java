/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.server.persists.repository;


import com.tarena.tp.luban.worker.server.bo.WorkerCategoryBO;
import com.tarena.tp.luban.worker.server.dao.WorkerCategoryDAO;
import com.tarena.tp.luban.worker.server.dto.param.WorkerCategoryParam;
import com.tarena.tp.luban.worker.server.persists.converter.WorkerCategoryConverter;
import com.tarena.tp.luban.worker.po.WorkerCategory;
import com.tarena.tp.luban.worker.server.repository.WorkerCategoryRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class WorkerCategoryRepositoryImpl implements WorkerCategoryRepository {
    @Resource
    private WorkerCategoryConverter workerCategoryConverter;

    @Resource
    private WorkerCategoryDAO workerCategoryDao;


    @Override
    public Long saveCategoryWorker(WorkerCategoryParam workerCategoryParam) {
        WorkerCategory workerCategory = this.workerCategoryConverter.param2po(workerCategoryParam);
        if (workerCategory.getId() != null) {
            this.workerCategoryDao.update(workerCategory);
            return workerCategory.getId();
        }
        this.workerCategoryDao.insert(workerCategory);
        return workerCategory.getId();
    }

    @Override public List<WorkerCategoryBO> getWorkerCategoryByWorkerId(Long workerCategoryId) {
        List<WorkerCategory> workerCategory = this.workerCategoryDao.getWorkerCategoryByWorkerId(workerCategoryId);
        return this.workerCategoryConverter.poList2BoList(workerCategory);
    }

    @Override
    public void deleteByUserId(Long userId) {
        workerCategoryDao.delete(userId);
    }

}