/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.server.persists.converter;

import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.worker.server.bo.WorkerAreaBO;
import com.tarena.tp.luban.worker.server.dto.param.WorkerAreaParam;
import com.tarena.tp.luban.worker.po.WorkerArea;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WorkerAreaConverter {


    public WorkerArea param2po(WorkerAreaParam param) {
        WorkerArea workerArea = new WorkerArea();
        BeanUtils.copyProperties(param, workerArea);
        LoginUser loginUser = SecurityContext.getLoginToken();
        workerArea.setGmtCreate(System.currentTimeMillis());
        workerArea.setGmtModified(workerArea.getGmtCreate());
        workerArea.setCreateUserId(loginUser != null ? loginUser.getUserId() : 0);
        workerArea.setModifiedUserId(loginUser != null ? loginUser.getUserId() : 0);
        workerArea.setCreateUserName(loginUser != null ? loginUser.getUserName() : "mock");
        workerArea.setModifiedUserName(loginUser != null ? loginUser.getUserName() : "mock");
        workerArea.setStatus(1);
        return workerArea;
    }

    public WorkerAreaBO po2bo(WorkerArea workerArea) {
        WorkerAreaBO workerAreaBO = new WorkerAreaBO();
        if (workerArea == null) {
            return workerAreaBO;
        }
        BeanUtils.copyProperties(workerArea, workerAreaBO);
        return workerAreaBO;
    }

    public List<WorkerAreaBO> poList2BoList(List<WorkerArea> list) {
        List<WorkerAreaBO> workerAreaBos = new ArrayList<>(list.size());
        for (WorkerArea workerArea : list) {
            workerAreaBos.add(this.po2bo(workerArea));
        }
        return workerAreaBos;
    }

}