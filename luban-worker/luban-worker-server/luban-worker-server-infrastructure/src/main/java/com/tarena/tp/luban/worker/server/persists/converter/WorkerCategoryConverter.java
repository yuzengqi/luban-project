/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.server.persists.converter;

import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.worker.server.bo.WorkerCategoryBO;
import com.tarena.tp.luban.worker.server.dto.param.WorkerCategoryParam;
import com.tarena.tp.luban.worker.po.WorkerCategory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WorkerCategoryConverter {

    public WorkerCategory param2po(WorkerCategoryParam param) {
        WorkerCategory workerCategory = new WorkerCategory();
        BeanUtils.copyProperties(param, workerCategory);
        LoginUser loginUser = SecurityContext.getLoginToken();
        workerCategory.setGmtCreate(System.currentTimeMillis());
        workerCategory.setGmtModified(workerCategory.getGmtCreate());
        workerCategory.setCreateUserId(loginUser != null ? loginUser.getUserId() : 0);
        workerCategory.setModifiedUserId(loginUser != null ? loginUser.getUserId() : 0);
        workerCategory.setCreateUserName(loginUser != null ? loginUser.getUserName() : "mock");
        workerCategory.setModifiedUserName(loginUser != null ? loginUser.getUserName() : "mock");
        workerCategory.setStatus(1);
        return workerCategory;
    }

    public WorkerCategoryBO po2bo(WorkerCategory workerCategory) {
        WorkerCategoryBO workerCategoryBO = new WorkerCategoryBO();
        if (workerCategory == null) {
            return workerCategoryBO;
        }
        BeanUtils.copyProperties(workerCategory, workerCategoryBO);
        return workerCategoryBO;
    }

    public List<WorkerCategoryBO> poList2BoList(List<WorkerCategory> list) {
        List<WorkerCategoryBO> workerCategoryBos = new ArrayList<>(list.size());
        for (WorkerCategory workerCategory : list) {
            workerCategoryBos.add(this.po2bo(workerCategory));
        }
        return workerCategoryBos;
    }

}