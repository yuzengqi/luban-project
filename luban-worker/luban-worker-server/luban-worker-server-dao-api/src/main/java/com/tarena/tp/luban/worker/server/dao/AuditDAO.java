package com.tarena.tp.luban.worker.server.dao;

import com.tarena.tp.luban.worker.po.WorkerAuditLog;

import java.util.List;

public interface AuditDAO {

    List<WorkerAuditLog> getAuditLog(Long workerId);
}
