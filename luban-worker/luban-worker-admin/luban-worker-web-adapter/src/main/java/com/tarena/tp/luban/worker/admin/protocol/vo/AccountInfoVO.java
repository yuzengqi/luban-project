package com.tarena.tp.luban.worker.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 师傅详情中的 账户信息
 */
@Data
public class AccountInfoVO {

    /**
     * 账户累计收入
     */
    @ApiModelProperty("账户累计收入")
    private Integer accountIncome;


    /**
     *  账户余额
     */
    @ApiModelProperty("账户余额")
    private Integer accountBalance;


    /**
     * 待收金额
     */
    @ApiModelProperty("待收金额")
    private Integer Amount;

    /**
     * 账户状态
     */
    @ApiModelProperty("账户状态")
    private String accountStatus;
}
