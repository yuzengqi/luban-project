package com.tarena.tp.luban.worker.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 审核意见
 */
@Data
public class AuditInfoVo {
    /**
     * 当前审核人  来自 passport 中的数据
     */
    @ApiModelProperty("当前审核人")
    private String operateName;

    /**
     * 审核时间
     */
    @ApiModelProperty("审核时间")
    private Long operateTime;

    /**
     *审核意见 1:通过  0:驳回
     */
    @ApiModelProperty("审核意见")
    private Integer auditStatus;

    /**
     * 驳回原因
     */
    @ApiModelProperty("驳回原因")
    private String rejectReason;

    /**
     * 备注信息
     */
    @ApiModelProperty("备注信息")
    private String remark;

    /**
     * 师傅 ID
     */
    @ApiModelProperty("师傅 ID")
    private Long workerId;
}
