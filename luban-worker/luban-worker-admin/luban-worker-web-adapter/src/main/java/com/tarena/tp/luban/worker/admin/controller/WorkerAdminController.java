package com.tarena.tp.luban.worker.admin.controller;

import com.tarena.tp.luban.worker.admin.assemble.WorkerAssemble;
import com.tarena.tp.luban.worker.admin.assemble.WorkerAuditAssemble;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.protocol.vo.AuditDetailVO;
import com.tarena.tp.luban.worker.admin.protocol.vo.WorkerVO;
import com.tarena.tp.luban.worker.admin.service.WorkerAdminService;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.model.Result;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/worker")
public class WorkerAdminController {
    @Autowired
    private WorkerAdminService workerAdminService;

    @Autowired
    private WorkerAssemble workerAssemble;

    @Autowired
    private WorkerAuditAssemble workerAuditAssemble;

    @PostMapping("/aduit")
    public Result<PagerResult<WorkerVO>> auditList(@RequestBody WorkerQuery workerQuery) {
        ListRecordTotalBO<WorkerBO> workerBOS = workerAdminService.auditList(workerQuery);
        PagerResult<WorkerVO> result = workerAssemble.assemblePagerResult(workerBOS, workerQuery);
        return new Result<>(result);
    }

    @PostMapping("/audit/detail")
    public Result<AuditDetailVO> auditDetail(@RequestParam(name = "workerId") Long userId) throws BusinessException {
        WorkerBO workerBo = workerAdminService.auditDetail(userId);
        AuditDetailVO auditDetailVO = workerAuditAssemble.assembleBO2DetailVO(workerBo);
        return new Result<>(auditDetailVO);
    }

    @PostMapping("/audit/save")
    public Result<Long> auditAdd(@RequestBody AuditParam auditParam) throws BusinessException {
        Long auditId = workerAdminService.auditAdd(auditParam);
        return new Result<>(auditId);
    }
}