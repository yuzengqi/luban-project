/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.admin.assemble;

import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.protocol.vo.*;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.pager.PagerResult;
import com.tedu.inn.protocol.pager.SimplePagerQuery;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class WorkerAssemble {
    public WorkerVO assembleBO2VO(WorkerBO bo) {
        WorkerVO worker = new WorkerVO();
        BeanUtils.copyProperties(bo, worker);
        return worker;
    }

    /**
     * WorkerDetailVO转换
     *
     * @param bo
     * @return
     */
    public WorkerDetailVO assembleBO2DetailVO(WorkerBO bo) {
        WorkerDetailVO workerDetailVO = new WorkerDetailVO();
        //师傅信息
        workerDetailVO.setWorkerInfoVO((assembleWorkerInfoVO(bo)));
        //认证信息
        workerDetailVO.setCertInfoVO(assembleCertInfo(bo));
        //银行卡信息
        workerDetailVO.setBankInfoVO(assembleBlankInfoVO(bo));
        //基础信息
        workerDetailVO.setBaseWorkerInfoVO(assembleBaseWorkerInfoVO(bo));
        //账户信息
        workerDetailVO.setAccountInfoVO(assembleAccountInfoVO(bo));
        return workerDetailVO;
    }

    /**
     * AccountInfoVO 转换
     */
    public AccountInfoVO assembleAccountInfoVO(WorkerBO bo) {
        AccountInfoVO accountInfoVO = new AccountInfoVO();
        BeanUtils.copyProperties(bo, accountInfoVO);
        return accountInfoVO;
    }

    /**
     * BaseWorkerInfoVO 转换
     */
    public BaseWorkerInfoVO assembleBaseWorkerInfoVO(WorkerBO bo) {
        BaseWorkerInfoVO baseWorkerInfoVO = new BaseWorkerInfoVO();
        BeanUtils.copyProperties(bo, baseWorkerInfoVO);
        return baseWorkerInfoVO;
    }

    /**
     * BlankInfoVO 转换
     */
    public BankInfoVO assembleBlankInfoVO(WorkerBO bo) {
        BankInfoVO blankInfoVO = new BankInfoVO();
        BeanUtils.copyProperties(bo, blankInfoVO);
        return blankInfoVO;
    }

    /**
     * CertInfoVO 转换
     */
    public CertInfoVO assembleCertInfo(WorkerBO bo) {
        CertInfoVO certInfoVO = new CertInfoVO();
        BeanUtils.copyProperties(bo, certInfoVO);
        return certInfoVO;
    }

    /**
     * WorkerInfoVO 转换
     */
    public WorkerInfoVO assembleWorkerInfoVO(WorkerBO bo) {
        WorkerInfoVO workerInfoVO = new WorkerInfoVO();
        BeanUtils.copyProperties(bo, workerInfoVO);
        return workerInfoVO;
    }

    public List<WorkerVO> boListAssembleVOList(List<WorkerBO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<WorkerVO> workerVOList = new ArrayList<>(list.size());
        for (WorkerBO workerBo : list) {
            WorkerVO workerVo = this.assembleBO2VO(workerBo);
            workerVOList.add(workerVo);
        }
        return workerVOList;
    }

    public PagerResult<WorkerVO> assemblePagerResult(ListRecordTotalBO<WorkerBO> workerListTotalRecord,
                                                     SimplePagerQuery workerQuery) {
        List<WorkerVO> workerVOList = this.boListAssembleVOList(workerListTotalRecord.getList());
        PagerResult<WorkerVO> pagerResult = new PagerResult<>(workerQuery);
        pagerResult.setObjects(workerVOList);
        pagerResult.setTotal(workerListTotalRecord.getTotal());
        return pagerResult;
    }
}