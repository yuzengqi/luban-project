package com.tarena.tp.luban.worker.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 师傅详情 passport 中的基础信息
 */
@Data
public class BaseWorkerInfoVO {

   /**
    * 邮箱地址
    */
   @ApiModelProperty("邮箱地址")
   private String email;

   /**
    * 注册日期
    */
   @ApiModelProperty("注册日期")
   private Long regDate;

   /**
    * 最后登录日期
    */
   @ApiModelProperty("最后登录日期")
   private Long logDate;


}
