package com.tarena.tp.luban.worker.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 审核详情
 * 审核内容:
 *  worker 表: real_name,IdCard,phone,categoryDetail,areaDetail
 * 审核意见:
 *  worker_audit_log 表 师傅审核日志表:
 *  审核人  从 passport 中获取
 *  审核时间 operate_time  新增时自动生成
 *  审核意见 audit_status 1:通过 2:驳回
 *  驳回原因  reject_reason
 *  备注信息  remark
 */
@Data
public class AuditDetailVO {

    /**
     * 审核内容
     */
    @ApiModelProperty("审核内容")
    private AuditWorkerInfoVO auditWorkerInfoVO;

    /**
     * 审核意见
     */
    @ApiModelProperty("审核意见")
    private List<AuditInfoVo> auditInfo;


}
