package com.tarena.tp.luban.worker.admin.bo;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
public class AuditBO {
    /**
     * 主键
     */
     Long id;

    /**
     * Passport 用户ID
     */
    Long userId;

    /**
     * 用户名称
     */
    String userName;

    /**
     * 师傅 ID
     */
    Long workerId;

    /**
     * 操作时间
     */
    Long operateTime;

    /**
     * 操作步骤
     */
    String operateName;

    /**
     * 状态 0 驳回, 1 通过
     */
    Integer auditStatus;

    /**
     * 驳回原因
     */
    String rejectReason;

    /**
     * 备注
     */
    String remark;

    /**
     * 创建人名称
     */
     String createUserName;
    /**
     * 创建人ID
     */
     Long createUserId;
    /**
     * 修改人ID
     */
     Long modifiedUserId;
    /**
     * 修改人姓名
     */
     String modifiedUserName;
    /**
     * 创建时间
     */
     Long gmtCreate;
    /**
     * 更新时间
     */
     Long gmtModified;

    /**
     * 审核内容
     */
//    AuditWorkerInfoBO auditWorkerInfoBO;

     String realName;

     String idCard;

     String phone;

     String categoryDetail;

     String areaDetail;

     AttachInfoBO cardUrl;



}
