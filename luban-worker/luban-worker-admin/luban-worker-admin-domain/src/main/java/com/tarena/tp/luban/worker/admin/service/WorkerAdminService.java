package com.tarena.tp.luban.worker.admin.service;

import com.github.xiaoymin.knife4j.core.util.CollectionUtils;
import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.worker.admin.bo.AuditBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.repository.AuditLogRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerAreaRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerCategoryRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerRepository;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WorkerAdminService {
    @Autowired(required = false)
    private WorkerRepository workerRepository;

    @Autowired(required = false)
    private WorkerAreaRepository workerAreaRepository;

    @Autowired(required = false)
    private WorkerCategoryRepository workerCategoryRepository;

    @Autowired(required = false)
    private AuditLogRepository auditLogRepository;

    @Autowired
    private AttachApi attachApi;

    @Autowired
    private AccountApi accountApi;

    public ListRecordTotalBO<WorkerBO> auditList(WorkerQuery workerQuery) {
        List<Integer> auditStatus = workerQuery.getAuditStatus();
        if (CollectionUtils.isEmpty(auditStatus)) {
            auditStatus.add(0);
            auditStatus.add(2);
        }
        Long total = workerRepository.getWorkerCount(workerQuery);
        List<WorkerBO> workerBOS = null;
        if (total > 0) {
            //获取师傅信息
            workerBOS = workerRepository.queryWorkers(workerQuery);
            for (WorkerBO workerBO : workerBOS) {
                //地址，地区信息
                Long userId = workerBO.getUserId();
                List<String> areaDetails = workerAreaRepository.getAreaDetails(userId);
                workerBO.setAreaDetails(areaDetails);
                List<String> categoryDetails = workerCategoryRepository.getCategoryDetails(userId);
                workerBO.setCategoryDetails(categoryDetails);
            }
        }
        return new ListRecordTotalBO<>(workerBOS, total);
    }

    public WorkerBO auditDetail(Long userId) throws BusinessException {
        WorkerBO worker = workerRepository.getWorker(userId);
        if (worker != null && worker.getUserId() != null) {
            List<String> areaDetails = workerAreaRepository.getAreaDetails(userId);
            worker.setAreaDetails(areaDetails);
            List<String> categoryDetails = workerCategoryRepository.getCategoryDetails(userId);
            worker.setCategoryDetails(categoryDetails);
            List<AuditBO> audit = auditLogRepository.getAudit(userId);
            worker.setWorkerAuditLogs(audit);
            //资质图片
            List<String> urls = getAttach(100, userId);
            worker.setAttachInfo(urls);
        }
        return worker;
    }

    private List<String> getAttach(Integer businessType, Long userId) {
        AttachQuery attachQuery = new AttachQuery();
        attachQuery.setBusinessType(businessType);
        attachQuery.setBusinessId(userId.intValue());
        List<String> attachUrls = new ArrayList<>();
        List<AttachDTO> attachDTOS = attachApi.getAttachInfoByParam(attachQuery);
        for (AttachDTO attachDTO : attachDTOS) {
            String urlPrefix = "http://localhost:8092/static/";
            String fileUuid = attachDTO.getFileUuid();
            String url = urlPrefix + fileUuid;
            attachUrls.add(url);
        }
        return attachUrls;
    }

    public Long auditAdd(AuditParam auditParam) throws BusinessException {
        WorkerBO workerBO = workerRepository.getWorker(auditParam.getWorkerId());
        Integer haveAuditStatus = workerBO.getAuditStatus();
        Integer aimAuditStatus = auditParam.getAuditStatus();
        checkStatusTransfer(haveAuditStatus, aimAuditStatus);
        //写入日志，返回id
        Long auditLogId = auditLogRepository.save(auditParam);
        WorkerParam workerParam = new WorkerParam();
        workerParam.setAuditStatus(aimAuditStatus);
        workerParam.setUserId(auditParam.getWorkerId());
        //修改状态值
        workerRepository.updateAuditStatus(workerParam);
        //创建账户
        if (workerBO.getCertStatus() == 0 && aimAuditStatus == 1) {
            //System.out.println("当前审核提交，暂时无法创建账户");
            AccountParam accountParam = new AccountParam();
            accountParam.setUserId(auditParam.getWorkerId());
            accountParam.setUserPhone(workerBO.getPhone());
            accountParam.setUserName(workerBO.getCreateUserName());
            Long result = accountApi.create(accountParam);
            if (result == 1) {
                //创建成功
                WorkerParam workerParam1 = new WorkerParam();
                workerParam1.setCertStatus(1);
                workerParam1.setUserId(workerBO.getUserId());
                workerRepository.updateCertStatus(workerParam1);
            }
        }
        return auditLogId;
    }

    private void checkStatusTransfer(Integer haveAuditStatus, Integer aimAuditStatus) throws BusinessException {
        if (aimAuditStatus == 2) {
            throw new BusinessException("6666", "资质流转不正常");
        }
        if (haveAuditStatus == 1 && aimAuditStatus == 0) {
            throw new BusinessException("6666", "资质流转不正常");
        }
    }
}
