package com.tarena.tp.luban.worker.admin.protocol.query;

import com.tedu.inn.protocol.pager.SimplePagerQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WorkerAuditQuery extends SimplePagerQuery {

    @ApiModelProperty("师傅编号")
    private String userId;

    @ApiModelProperty("师傅姓名")
    private String realName; //真实姓名 real_name

    @ApiModelProperty("师傅手机号")
    private String phone;  //手机号 phone

    @ApiModelProperty("身份证号码")
    private String IdCard;

    @ApiModelProperty("审核状态 0:驳回  1:通过   2:未审核")
    private Integer auditStatus;

    @ApiModelProperty("开始日期")
    private Long startDate;

    @ApiModelProperty("结束日期")
    private Long endDate;
}
