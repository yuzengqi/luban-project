package com.tarena.tp.luban.worker.admin.infrastructure.persistence.impl;

import com.tarena.tp.luban.worker.admin.bo.WorkerAreaBO;
import com.tarena.tp.luban.worker.admin.dao.WorkerAreaDAO;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.WorkerAreaConverter;
import com.tarena.tp.luban.worker.admin.repository.WorkerAreaRepository;
import com.tarena.tp.luban.worker.po.WorkerArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class WorkerAreaRepositoryImpl implements WorkerAreaRepository {
    @Autowired
    private WorkerAreaDAO workerAreaDAO;

    @Autowired
    private WorkerAreaConverter workerAreaConverter;

    @Override
    public List<WorkerAreaBO> getWorkerArea(Long workerAreaId) {
        return null;
    }

    @Override
    public List<String> getAreaDetails(Long userId) {
        List<WorkerArea> workerAreas = workerAreaDAO.getWorkerAreaByUserId(userId);
        List<String> areaDetails = new ArrayList<>();
        for (WorkerArea workerArea : workerAreas) {
            areaDetails.add(workerArea.getAreaDetail());
        }
        return areaDetails;
    }
}
