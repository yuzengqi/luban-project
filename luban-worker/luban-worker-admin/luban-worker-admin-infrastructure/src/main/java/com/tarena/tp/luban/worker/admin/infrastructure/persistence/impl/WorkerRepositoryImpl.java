package com.tarena.tp.luban.worker.admin.infrastructure.persistence.impl;

import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.dao.WorkerDAO;
import com.tarena.tp.luban.worker.admin.dao.query.WorkerDBPagerQuery;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.WorkerConverter;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.repository.WorkerRepository;
import com.tarena.tp.luban.worker.po.Worker;
import com.tedu.inn.protocol.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WorkerRepositoryImpl implements WorkerRepository {
    @Autowired
    private WorkerConverter workerConverter;

    @Autowired(required = false)
    private WorkerDAO workerDAO;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public WorkerBO getWorker(Long workerId) throws BusinessException {
        Worker worker = workerDAO.getEntity(workerId);
        WorkerBO workerBO = workerConverter.po2bo(worker);
        return workerBO;
    }

    @Override
    public Long getWorkerCount(WorkerQuery workerQuery) {
        WorkerDBPagerQuery workerDBPagerQuery = workerConverter.toDbPagerQuery(workerQuery);
        return workerDAO.countWorker(workerDBPagerQuery);
    }

    @Override
    public List<WorkerBO> queryWorkers(WorkerQuery workerBOQuery) {
        WorkerDBPagerQuery workerDBPagerQuery = workerConverter.toDbPagerQuery(workerBOQuery);
        List<Worker> workers = workerDAO.queryWorkers(workerDBPagerQuery);
        List<WorkerBO> workerBOS = workerConverter.poList2BoList(workers);
        return workerBOS;
    }

    @Override
    public Integer disable(Long bankIds) {
        return null;
    }

    @Override
    public Integer enable(Long bankIds) {
        return null;
    }

    @Override
    public void updateAuditStatus(WorkerParam workerParam) {
        //注意缓存一致性
        Worker worker = workerConverter.param2po(workerParam);
        workerDAO.updateWorkerAuditStatus(worker);
        //删除缓存
        String key = "luban:worker:" + workerParam.getUserId();
        redisTemplate.delete(key);
    }

    @Override
    public void updateCertStatus(WorkerParam workerParam) {
        //因为师傅信息改变了，注意缓存一致性
        Worker worker = workerConverter.param2po(workerParam);
        workerDAO.updateWorkerCertStatus(worker);
        String key = "luban:worker:" + workerParam.getUserId();
        redisTemplate.delete(key);
    }
}
