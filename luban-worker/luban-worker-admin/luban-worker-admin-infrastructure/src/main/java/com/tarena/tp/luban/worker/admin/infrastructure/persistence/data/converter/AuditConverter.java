package com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter;

import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.worker.admin.bo.AuditBO;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.po.WorkerAuditLog;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class AuditConverter {

    /**
     * 审核参数对象转审核实体对象
     * Param--->Po
     * @param param
     * @return
     */
    public WorkerAuditLog param2po(AuditParam param) {
        WorkerAuditLog audit = new WorkerAuditLog();
        BeanUtils.copyProperties(param, audit);
        //登录用户 审核操作的登录用户,管理员
        LoginUser loginUser = SecurityContext.getLoginToken();
        audit.setOperateName(loginUser.getUserName());
        audit.setOperateTime(System.currentTimeMillis());
        audit.setGmtCreate(System.currentTimeMillis());
        audit.setGmtModified(audit.getGmtCreate());
        audit.setCreateUserId(loginUser.getUserId());
        audit.setModifiedUserId(loginUser.getUserId());
        audit.setCreateUserName(loginUser.getUserName());
        audit.setModifiedUserName(loginUser.getUserName());
        return audit;
    }

    /**
     * 审核实体对象转换审核 BO
     * PO--BO
     * @param auditLog
     * @return
     */
    public AuditBO po2bo(WorkerAuditLog auditLog) {
        AuditBO auditBO = new AuditBO();
        BeanUtils.copyProperties(auditLog, auditBO);
        return auditBO;
    }

    public List<AuditBO> pos2bos(List<WorkerAuditLog> auditLog) {
        if (CollectionUtils.isEmpty(auditLog)) {
            return Collections.emptyList();
        }
        List<AuditBO> result = new ArrayList<>();
        for (WorkerAuditLog log : auditLog) {
            result.add(po2bo(log));
        }
        return result;
    }
}
