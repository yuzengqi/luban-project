package com.tarena.tp.luban.worker.admin.infrastructure.persistence.impl;

import com.tarena.tp.luban.worker.admin.bo.WorkerCategoryBO;
import com.tarena.tp.luban.worker.admin.dao.WorkerCategoryDAO;
import com.tarena.tp.luban.worker.admin.repository.WorkerCategoryRepository;
import com.tarena.tp.luban.worker.po.WorkerCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class WorkerCategoryRepositoryImpl implements WorkerCategoryRepository {
    @Autowired
    private WorkerCategoryDAO workerCategoryDAO;

    @Override
    public List<WorkerCategoryBO> getWorkerCategory(Long workerCategoryId) {
        return null;
    }

    @Override
    public List<String> getCategoryDetails(Long userId) {
        List<WorkerCategory> workerCategories = workerCategoryDAO.getCategoryByUserId(userId);
        List<String> categoryDetails = new ArrayList<>();
        for (WorkerCategory workerCategory : workerCategories) {
            categoryDetails.add(workerCategory.getCategoryDetail());
        }
        return categoryDetails;
    }
}
