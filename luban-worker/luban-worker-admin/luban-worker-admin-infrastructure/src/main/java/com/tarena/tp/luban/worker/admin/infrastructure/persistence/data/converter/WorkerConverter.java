/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter;

import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.po.WorkerAuditLog;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.dao.query.WorkerDBPagerQuery;
import org.springframework.beans.BeanUtils;
import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import java.util.ArrayList;
import java.util.List;
import com.tarena.tp.luban.worker.po.*;
import org.springframework.stereotype.Component;

@Component
public class WorkerConverter {

    public WorkerDBPagerQuery toDbPagerQuery(WorkerQuery workerQuery) {
        if (workerQuery == null) {
            return new WorkerDBPagerQuery();
        }
        WorkerDBPagerQuery workerDBPagerQuery = new WorkerDBPagerQuery();
        BeanUtils.copyProperties(workerQuery, workerDBPagerQuery);
        return workerDBPagerQuery;
    }

    /**
     * 新增师傅
     * @param param
     * @return
     */
    public Worker param2po(WorkerParam param) {
        Worker worker = new Worker();
        BeanUtils.copyProperties(param, worker);

        LoginUser loginUser = SecurityContext.getLoginToken();
        worker.setGmtCreate(System.currentTimeMillis());
        worker.setGmtModified(worker.getGmtCreate());
        worker.setCreateUserId(loginUser.getUserId());
        worker.setModifiedUserId(loginUser.getUserId());
        worker.setStatus(1);

        worker.setCreateUserName(loginUser.getUserName());
        worker.setModifiedUserName(loginUser.getUserName());
        return worker;
    }


    public WorkerBO po2bo(Worker worker) {
        WorkerBO workerBO = new WorkerBO();
        if (worker == null) {
            return workerBO;
        }
        BeanUtils.copyProperties(worker, workerBO);
        return workerBO;
    }

    public List<WorkerBO> poList2BoList(List<Worker> list) {
        List<WorkerBO> workerBos = new ArrayList<>(list.size());
        for (Worker worker : list) {
            workerBos.add(this.po2bo(worker));
        }
        return workerBos;
    }

    public void convertStatus(StatusCriteria statusCriteria) {
        LoginUser loginUser = SecurityContext.getLoginToken();
        statusCriteria.setModifiedUserId(loginUser.getUserId());
        statusCriteria.setModifiedUserName(loginUser.getUserName());
        statusCriteria.setGmtModified(System.currentTimeMillis());
    }
}