package com.tarena.tp.luban.worker.admin.dao;

import com.tarena.tp.luban.worker.po.WorkerAuditLog;

import java.util.List;

public interface AuditDAO {

    Integer update(WorkerAuditLog auditLog);

    Long insert(WorkerAuditLog auditLog);

    WorkerAuditLog getEntity(Long workerId);

    List<WorkerAuditLog> getAuditLog(Long workerId);
}
