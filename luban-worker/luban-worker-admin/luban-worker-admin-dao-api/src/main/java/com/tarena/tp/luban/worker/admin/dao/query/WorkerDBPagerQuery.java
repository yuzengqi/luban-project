/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.admin.dao.query;

import java.util.Date;
import java.util.List;

import com.tedu.inn.protocol.dao.DatabasePagerQuery;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class WorkerDBPagerQuery extends DatabasePagerQuery {
    //父类 有2个属性 pageNO=1 pageSize=10=rows offSet=(pageNO-1)*rows
    //select * from worker limit 0,10 从第0条开始 一直查询 总共10条 0-9 查询第一页的十条数据
    public WorkerDBPagerQuery() {
    }
    private String userId;

    private String realName; //真实姓名 real_name

    private String phone;  //手机号 phone

    private String idCard;

    private Integer status;  //账号状态

    private List<Integer> auditStatus;  //审核状态

    private Integer certStatus;   //认证状态 null

    private Long startDate;

    private Long endDate;
    private String limit;//"limit offSet,rows"

}